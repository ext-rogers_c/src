#include <stdlib.h>

#include "gtest/gtest.h"
#include "Fields/Interpolation/LeastSquaresSolveFactory.h"
#include "Fields/Interpolation/SquarePolynomialVector.h"
#include "Fields/Interpolation/PolynomialCoefficient.h"
#include "Fields/Interpolation/MMatrix.h"
#include "Utilities/GeneralClassicException.h"

#include "opal_test_utilities/SilenceTest.h"

using interpolation::SquarePolynomialVector;
using interpolation::LeastSquaresSolveFactory;
using interpolation::MMatrix;
using interpolation::PolynomialCoefficient;

class LeastSquaresSolveFactoryTest : public ::testing::Test {
  public:
    LeastSquaresSolveFactoryTest() {
    }

    SquarePolynomialVector getMap(size_t pointDim, size_t valueDim, size_t order);
    OpalTestUtilities::SilenceTest silencer;

    std::vector< std::vector<double> >  getPoints(size_t pointDim);
    std::vector< std::vector<double> >  getValues(
                                    SquarePolynomialVector ref,
                                    std::vector< std::vector<double> > points);
};

SquarePolynomialVector LeastSquaresSolveFactoryTest::getMap
                              (size_t pointDim, size_t valueDim, size_t order) {
    size_t ncols = SquarePolynomialVector::NumberOfPolynomialCoefficients(pointDim, order);
    std::vector<double> pos(valueDim*ncols, 0.);
    for (size_t i = 0; i < pos.size(); ++i) {
        pos[i] = i+1;
    }
    MMatrix<double> matrix(valueDim, ncols, &pos[0]);
    SquarePolynomialVector pv(pointDim, matrix);
    return pv;
}

std::vector< std::vector<double> >  LeastSquaresSolveFactoryTest::getPoints(
                                                              size_t pointDim) {
    std::vector< std::vector<double> > points;
    for (size_t i = 0; i < 20; ++i) {
        points.push_back(std::vector<double>(pointDim));
        for (size_t j = 0; j < pointDim; ++j) {
            points[i][j] = rand()%1000/1000.;
        }
    }
    return points;
}

std::vector< std::vector<double> >  LeastSquaresSolveFactoryTest::getValues(
                              SquarePolynomialVector ref,
                              std::vector< std::vector<double> > points) {
    std::vector< std::vector<double> > values;
    for (size_t i = 0; i < points.size(); ++i) {
        values.push_back(std::vector<double>(ref.ValueDimension()));
        ref.F(&points[i][0], &values[i][0]);
    }
    return values;
}

TEST_F(LeastSquaresSolveFactoryTest, testSolverNoCoefficients) {
    size_t pointDim = 2;
    size_t valueDim = 3;
    size_t order = 2;
    SquarePolynomialVector ref = getMap(pointDim, valueDim, order);
    std::vector< std::vector<double> > points = getPoints(pointDim);
    std::vector< std::vector<double> > values = getValues(ref, points);
    LeastSquaresSolveFactory solver(order, points);
    SquarePolynomialVector test = solver.solve(values);
    for (size_t i = 0; i < 3; ++i) {
        for (size_t j = 0; j < 9; ++j) {
            EXPECT_NEAR(ref.GetCoefficientsAsMatrix()(i+1, j+1),
                        test.GetCoefficientsAsMatrix()(i+1, j+1), 1e-6);
        }
    }
    std::cerr << ref << std::endl;
    std::cerr << test << std::endl;
}

TEST_F(LeastSquaresSolveFactoryTest, testCtorErrors) {
    std::vector< std::vector<double> > points;
    EXPECT_THROW(LeastSquaresSolveFactory solver(1, points), GeneralClassicException);
    points.push_back(std::vector<double>());
    EXPECT_THROW(LeastSquaresSolveFactory solver(1, points), GeneralClassicException);
    points[0] = std::vector<double>(1, 0.);
    points.push_back(std::vector<double>(2, 0.));
    points.push_back(std::vector<double>(1, 0.));
    EXPECT_THROW(LeastSquaresSolveFactory solver(1, points), GeneralClassicException);
}

TEST_F(LeastSquaresSolveFactoryTest, testSolveErrors) {
    std::vector< std::vector<double> > points(3, std::vector<double>(3, 0.));
    LeastSquaresSolveFactory solver(1, points);
    std::vector< std::vector<double> > values1(2, std::vector<double>(1, 0.));
    EXPECT_THROW(solver.solve(values1), GeneralClassicException);
    std::vector< std::vector<double> > values2(3, std::vector<double>());
    EXPECT_THROW(solver.solve(values2), GeneralClassicException);
    std::vector< std::vector<double> > values3(2, std::vector<double>(2, 0.));
    values3.push_back(std::vector<double>(3, 0.));
    EXPECT_THROW(solver.solve(values3), GeneralClassicException);
}

TEST_F(LeastSquaresSolveFactoryTest, testSolveErrors2) {
    std::vector< std::vector<double> > points(1, std::vector<double>(2, 0.));
    std::vector< std::vector<double> > values(1, std::vector<double>(3, 0.));
    LeastSquaresSolveFactory solver(2, points);
    // under constrained; matrix invert should fail
    EXPECT_THROW(solver.solve(values), GeneralClassicException);
}
    
TEST_F(LeastSquaresSolveFactoryTest, testCoefficients1) {
    size_t pointDim = 2;
    size_t valueDim = 3;
    size_t order = 2;
    SquarePolynomialVector ref = getMap(pointDim, valueDim, order);
    std::vector< std::vector<double> > points = getPoints(pointDim);
    std::vector< std::vector<double> > values = getValues(ref, points);
    LeastSquaresSolveFactory solver(order, points);
    PolynomialCoefficient a_coeff(std::vector<int>{0, 1}, 2, 44.0);
    std::vector<PolynomialCoefficient> coeff(1, a_coeff);
    solver.setCoefficients(coeff);
    SquarePolynomialVector test = solver.solve(values);
    // because we treat each axis in codomain indepently, the 0 and 1 dimension
    // should be exactly solved
    for (size_t i = 0; i < 2; ++i) {
        for (size_t j = 0; j < 9; ++j) {
            EXPECT_NEAR(ref.GetCoefficientsAsMatrix()(i+1, j+1),
                        test.GetCoefficientsAsMatrix()(i+1, j+1), 1e-6);
        }
    }
    // the 2 dimension should have 1,1 fixed but the rest a mess
    EXPECT_NEAR(test.GetCoefficientsAsMatrix()(3, 4), 44.0, 1e-6);
    std::cerr << ref << std::endl;
    std::cerr << test << std::endl;
}

TEST_F(LeastSquaresSolveFactoryTest, testCoefficients2) {
    size_t pointDim = 2;
    size_t valueDim = 3;
    size_t order = 2;
    SquarePolynomialVector ref = getMap(pointDim, valueDim, order);
    std::vector< std::vector<double> > points = getPoints(pointDim);
    std::vector< std::vector<double> > values = getValues(ref, points);
    LeastSquaresSolveFactory solver(order, points);
    std::vector<PolynomialCoefficient> coeff;
    coeff.push_back(PolynomialCoefficient(std::vector<int>{}, 0, 1.0));
    coeff.push_back(PolynomialCoefficient(std::vector<int>{0, 0}, 0, 13.0));
    coeff.push_back(PolynomialCoefficient(std::vector<int>{0, 1}, 2, 12.0));
    coeff.push_back(PolynomialCoefficient(std::vector<int>{0, 0, 1, 1}, 2, 27.0));
    solver.setCoefficients(coeff);
    SquarePolynomialVector test;
    test = solver.solve(values);
    for (size_t i = 0; i < 3; ++i) {
        for (size_t j = 0; j < 9; ++j) {
            EXPECT_NEAR(ref.GetCoefficientsAsMatrix()(i+1, j+1),
                        test.GetCoefficientsAsMatrix()(i+1, j+1), 1e-6);
        }
    }
    std::cerr << ref << std::endl;
    std::cerr << test << std::endl;
}

TEST_F(LeastSquaresSolveFactoryTest, testCoefficientsErrors) {
    size_t pointDim = 2;
    size_t valueDim = 3;
    size_t order = 2;
    SquarePolynomialVector ref = getMap(pointDim, valueDim, order);
    std::vector< std::vector<double> > points = getPoints(pointDim);
    std::vector< std::vector<double> > values = getValues(ref, points);
    LeastSquaresSolveFactory solver(order, points);
    std::vector<PolynomialCoefficient> coeff;
    // polynomial order too high
    coeff.push_back(PolynomialCoefficient(std::vector<int>{0, 0, 0}, 0, 1.0));
    EXPECT_THROW(solver.setCoefficients(coeff), GeneralClassicException);

    // point dimension too high
    coeff[0] = PolynomialCoefficient(std::vector<int>{2}, 0, 1.0);
    EXPECT_THROW(solver.setCoefficients(coeff), GeneralClassicException);

    // value dimension too high; can only check at solve time
    coeff[0] = PolynomialCoefficient(std::vector<int>{0}, 4, 1.0);
    solver.setCoefficients(coeff);
    EXPECT_THROW(solver.solve(values), GeneralClassicException);
}

TEST_F(LeastSquaresSolveFactoryTest, testWeights) {
    size_t pointDim = 2;
    size_t valueDim = 3;
    size_t order = 2;
    SquarePolynomialVector ref = getMap(pointDim, valueDim, order);
    std::vector< std::vector<double> > points = getPoints(pointDim);
    std::vector< std::vector<double> > values = getValues(ref, points);
    points.push_back(points.back());
    values.push_back(std::vector<double>(3, 0.));
    LeastSquaresSolveFactory solver(order, points);
    SquarePolynomialVector test;
    test = solver.solve(values); // should be very far from ref
    std::vector<double> weights(points.size(), 1.);
    weights.back() = 0;
    solver.setWeights(weights);
    test = solver.solve(values); // should be same as ref
    for (size_t i = 0; i < 3; ++i) {
        for (size_t j = 0; j < 9; ++j) {
            EXPECT_NEAR(ref.GetCoefficientsAsMatrix()(i+1, j+1),
                        test.GetCoefficientsAsMatrix()(i+1, j+1), 1e-6);
        }
    }
    std::cerr << ref << std::endl;
    std::cerr << test << std::endl;
}

TEST_F(LeastSquaresSolveFactoryTest, testWeightsErrors) {
    size_t pointDim = 2;
    size_t valueDim = 3;
    size_t order = 2;
    SquarePolynomialVector ref = getMap(pointDim, valueDim, order);
    std::vector< std::vector<double> > points = getPoints(pointDim);
    std::vector< std::vector<double> > values = getValues(ref, points);
    LeastSquaresSolveFactory solver(order, points);
    SquarePolynomialVector test;
    test = solver.solve(values); // should be very far from ref
    std::vector<double> weights(points.size()-1, 1.);
    EXPECT_THROW(solver.setWeights(weights), GeneralClassicException);
}


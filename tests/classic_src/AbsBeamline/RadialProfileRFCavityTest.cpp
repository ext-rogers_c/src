/*
 *  Copyright (c) 2019, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include "gtest/gtest.h"

#include "Physics/Physics.h"
#include "AbsBeamline/RadialProfileRFCavity.h"

class RadialProfileRFCavityTest : public ::testing::Test {
 protected:
  RadialProfileRFCavityTest() {}
  virtual ~RadialProfileRFCavityTest() {}
  virtual void SetUp() {}
  virtual void TearDown() {}
};

void testCavity(RadialProfileRFCavity& rf, std::string id) {
    EXPECT_NEAR(rf.getHeight(), 2., 1e-12) << id;
    EXPECT_NEAR(rf.getLength(), 3., 1e-12) << id;
    EXPECT_NEAR(rf.getFrequency(), 4., 1e-12) << id;
    EXPECT_NEAR(rf.getPhase(), 5., 1e-12) << id;
    std::vector<std::pair<double, double> > profile = rf.getAmplitudeProfile();
    ASSERT_EQ(int(profile.size()), 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_NEAR(profile[i].first, i+1., 1e-12);
        EXPECT_NEAR(profile[i].second, (i+2.)*10, 1e-12);
    }
}

TEST_F(RadialProfileRFCavityTest, TestConstructorEtc) {
    std::vector<std::pair<double, double> > profile = 
                                {{1., 20.}, {2., 30.}, {3., 40.}, {4., 50.}};
    RadialProfileRFCavity rf1;
    rf1.setHeight(2.);
    rf1.setLength(3.);
    rf1.setFrequency(4.);
    rf1.setPhase(5.);
    rf1.setAmplitudeProfile(profile);
    testCavity(rf1, "Default");
    RadialProfileRFCavity rf2(rf1);
    testCavity(rf2, "Copy");
    RadialProfileRFCavity rf3;
    rf3.getHeight();
    rf3 = rf2;
    testCavity(rf3, "Assignment");
    RadialProfileRFCavity* rf4 =
                              dynamic_cast<RadialProfileRFCavity*>(rf3.clone());
    testCavity(*rf4, "clone");
    
}
 
TEST_F(RadialProfileRFCavityTest, TestApply) {
    RadialProfileRFCavity rf1;
    rf1.setHeight(2.);
    rf1.setLength(3.);
    rf1.setFrequency(4.);
    rf1.setPhase(Physics::pi/2.);
    std::vector<std::pair<double, double> > profile;
    for (size_t i = 0; i < 101; ++i) {
        std::pair<double, double> r_amp(i/50.-1.0, i*i);
        profile.push_back(r_amp);
    }
    rf1.setAmplitudeProfile(profile);
    rf1.initialise();
    Vector_t R(0., 0, 0.5);
    Vector_t P, E, B;
    double t = 0;
    for (size_t i = 0; i < 100; ++i) {
        R[0] = (i+0.5)/50.-1.0;
        bool outOfBounds = rf1.apply(R, P, t, E, B);
        EXPECT_FALSE(outOfBounds);
        EXPECT_GT(E[2], profile[i].second);
        EXPECT_LT(E[2], profile[i+1].second);
    }   
}
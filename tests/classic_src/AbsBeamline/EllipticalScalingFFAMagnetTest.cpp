/*
 *  Copyright (c) 2017, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include <cmath>
#include <fstream>
#include <sstream>

#include "gtest/gtest.h"
#include "opal_test_utilities/SilenceTest.h"

#include "Classic/AbsBeamline/EndFieldModel/Tanh.h"
#include "Classic/AbsBeamline/Offset.h"
#include "Physics/Physics.h"
#include "Classic/AbsBeamline/EllipticalScalingFFAMagnet.h"

class EllipticalScalingFFAMagnetTest : public ::testing::Test {
public:
    EllipticalScalingFFAMagnetTest() : sector_m(NULL), fout_m() {
    }

    void SetUp( ) {
        sector_m = new EllipticalScalingFFAMagnet("test");
        // characteristic length is R*dphi => 0.6545 m
        endfieldmodel::Tanh* tanh = new endfieldmodel::Tanh(nu0_m, nu0_m/5., 20);
        sector_m->setEndField(tanh);
        sector_m->setTanDelta(0.);
        sector_m->setMu0(mu0_m);
        sector_m->setMuMin(mu0_m/4.);
        sector_m->setMuMax(mu0_m*2.1);
        sector_m->setA(a_m);
        sector_m->setDipoleConstant(1.);
        sector_m->setFieldIndex(15);
        sector_m->setMaxOrder(10);
        sector_m->setNuStart(nu0_m);
        sector_m->setNuEnd(nu0_m*4.);
        sector_m->setAzimuthalExtent(M_PI);
        sector_m->setVerticalExtent(1.); // 1 m
        sector_m->initialise();
    }

    void TearDown( ) {
        delete sector_m;
        sector_m = NULL;
    }

    ~EllipticalScalingFFAMagnetTest() {
    }

    EllipticalScalingFFAMagnet* sector_m;
    std::ofstream fout_m;
    double mu0_m = 0.8; // m
    double magnetLength_m = 0.02; // m
    double nu0_m = magnetLength_m*2*M_PI/mu0_m; // radians
    double a_m = 2;
    
private:
    //OpalTestUtilities::SilenceTest silencer_m;
};

TEST_F(EllipticalScalingFFAMagnetTest, TestConstructor) {
    Vector_t centre = sector_m->getCentre();
    EXPECT_NEAR(centre[0], -a_m*cosh(mu0_m), 1e-12);
    EXPECT_NEAR(centre[1], 0., 1e-12);
    EXPECT_NEAR(centre[2], 0., 1e-12);
}

TEST_F(EllipticalScalingFFAMagnetTest, TestCoordinateTransformationsFore) {
    double mu, nu, x_out, y_out;
    for (double theta = 0.; theta < 3*Physics::pi+0.1; theta += Physics::pi/10.) {
        double x_in=cos(theta), y_in=sin(theta);
        sector_m->convertToElliptical(x_in, y_in, mu, nu);
        sector_m->convertToCartesian(mu, nu, x_out, y_out);
        EXPECT_NEAR(x_in, x_out, 1e-12);
        EXPECT_NEAR(y_in, y_out, 1e-12);
    }
}

TEST_F(EllipticalScalingFFAMagnetTest, TestCoordinateTransformationsBack) {
    double mu_in=1., nu_out, mu_out, x ,y;
    for (double nu_in = -Physics::pi; nu_in < 3*Physics::pi+0.1; nu_in += Physics::pi/10.) {
        sector_m->convertToCartesian(mu_in, nu_in, x, y);
        sector_m->convertToElliptical(x, y, mu_out, nu_out);
        std::cerr << mu_in << " "  << std::setw(12) << nu_in << " "
                  << std::setw(12) << x << " " << std::setw(12) << y << " "
                  << std::endl;
        EXPECT_NEAR(mu_in, mu_out, 1e-12);
        if (nu_in < 0) {
            nu_out -= 2.*Physics::pi;
        } else if (nu_in > 2.*Physics::pi) {
            nu_out += 2.*Physics::pi;
        }
        EXPECT_NEAR(nu_in, nu_out, 1e-12);
    }
}

TEST_F(EllipticalScalingFFAMagnetTest, TestGetFieldValueEllipticalMu) { // scan mu
    double x, y;
    for (double i = 0; i < 1.01; i += 0.1) {
        double nu = nu0_m;
        double mu = mu0_m*i+mu0_m/2.;
        Vector_t bEll, bCar;
        double test = sector_m->getDipoleConstant()*pow(mu/mu0_m, 15)*sector_m->getEndField()->function(nu-nu0_m, 0.);
        sector_m->convertToCartesian(mu, nu, x, y);
        Vector_t posCar = Vector_t(x, 0., y)+sector_m->getCentre();
        sector_m->getFieldValueElliptical(Vector_t(mu, 0., nu), bEll);
        sector_m->getFieldValue(posCar, bCar);
        std::cerr << i << " " << bEll[1] << " " << bCar[1] << " " << test << std::endl;
        /*EXPECT_NEAR(bEll[0], 0., 1e-12);
        EXPECT_NEAR(bEll[1]/test, 1., 1e-12);
        EXPECT_NEAR(bEll[2], 0., 1e-12);
        EXPECT_NEAR(bCar[0], 0., 1e-12);
        EXPECT_NEAR(bCar[1]/test, 1., 1e-12);
        EXPECT_NEAR(bCar[2], 0., 1e-12);*/
    }
}

TEST_F(EllipticalScalingFFAMagnetTest, TestGetFieldValueEllipticalNu) { // scan nu
    double x, y;
    std::cerr << sector_m->getCentre() << std::endl;
    for (double i = -1.001; i < 2.01; i += 0.1) {
        double mu = mu0_m;
        double nu = i*nu0_m*2;
        Vector_t bEll, bCar;
        sector_m->convertToCartesian(mu, nu, x, y);
        if (nu < 0.) {
            // nu should be in domain 0 < 2 pi for elliptical lookup
            nu += 2.*Physics::pi;
        } else if (nu > 2.*Physics::pi) {
            nu -= 2.*Physics::pi;
        }
        Vector_t posCar = Vector_t(x, 0., y)+sector_m->getCentre();
        sector_m->getFieldValueElliptical(Vector_t(mu, 0., nu), bEll);
        sector_m->getFieldValue(posCar, bCar);
        double test = 0.;
        if (nu >= 0. && nu <= sector_m->getNuEnd()) {
            test = sector_m->getEndField()->function(nu-nu0_m, 0.);
        }
        std::cerr << "Coordinates " 
                    << std::setw(12) << mu << " " 
                    << std::setw(12) << nu << " " 
                    << std::setw(12) << x << " " 
                    << std::setw(12) << y << " "
                    << std::setw(12) << posCar[0] << " " 
                    << std::setw(12) << posCar[2] << " " << std::endl;
        std::cerr << "  " << i << " " 
                  << std::setw(12) << bEll[1] << " " 
                  << std::setw(12) << bCar[1] << " " 
                  << std::setw(12) << test << std::endl;
        /*EXPECT_NEAR(bEll[0], 0., 1e-12);
        EXPECT_NEAR(bEll[1], test, 1e-12);
        EXPECT_NEAR(bEll[2], 0., 1e-12);
        EXPECT_NEAR(bCar[0], 0., 1e-12);
        EXPECT_NEAR(bCar[1], test, 1e-12);
        EXPECT_NEAR(bCar[2], 0., 1e-12);*/
    }
}

TEST_F(EllipticalScalingFFAMagnetTest, TestBoundingBox) {
    double x, y;

    std::vector<double> nuVec({
        -0.001,
         0.001,
         0.001,
        -0.001,
         0.001,
         0.001,
         sector_m->getNuEnd()-0.001,
         sector_m->getNuEnd()+0.001});
    std::vector<double> muVec({
        mu0_m/4.+0.001,
        mu0_m/4.-0.001,
        mu0_m/4.+0.001,
        2.1*mu0_m-0.001,
        2.1*mu0_m+0.001,
        2.1*mu0_m-0.001,
        mu0_m,
        mu0_m});
    std::vector<bool> boundVec({
        true, true, false, // min mu, min nu corner
        true, true, false, // max mu, min nu corner
        false, true});
    Vector_t bCar, posCar;
    for (size_t i = 0; i < nuVec.size(); ++i) {
        sector_m->convertToCartesian(muVec[i], nuVec[i], x, y);
        double mu, nu;
        sector_m->convertToElliptical(x, y, mu, nu);
        posCar = Vector_t(x, 0., y)+sector_m->getCentre();
        bool oob = sector_m->getFieldValue(posCar, bCar);
        EXPECT_EQ(oob, boundVec[i]) << "i: " << i << " nu: " << nuVec[i]
                                    << " mu: " << muVec[i];
        std::cerr << "TESTY " << mu << " " << nu << std::endl;
        
    }
}

TEST_F(EllipticalScalingFFAMagnetTest, TestDomainError) {
    EXPECT_TRUE(false) << "Should raise an error if domain of the field is not valid";
}

TEST_F(EllipticalScalingFFAMagnetTest, TestOffMidplane) {
    EXPECT_TRUE(false) << "Should calculate field off the midplane correctly (Maxwellian)";
}

TEST_F(EllipticalScalingFFAMagnetTest, TestGeometry) {
    EXPECT_TRUE(false) << "Should create geometry object in correct (elliptical) geometry";
}


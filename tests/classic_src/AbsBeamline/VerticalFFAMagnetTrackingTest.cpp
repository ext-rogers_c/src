#include <iomanip>

#include "gtest/gtest.h"

#include "Classic/AbsBeamline/EndFieldModel/Tanh.h"
#include "classic_src/AbsBeamline/TrackingTest/Tracking.h"
#include "classic_src/AbsBeamline/TrackingTest/BuildRing.h"
#include "classic_src/AbsBeamline/TrackingTest/BuildLine.h"
#include "Classic/AbsBeamline/Ring.h"

#include "Classic/AbsBeamline/VerticalFFAMagnet.h"


class VerticalFFAMagnetTrackingTest : public ::testing::Test {
public:
    VerticalFFAMagnetTrackingTest() {
    }

    void SetUp( ) {
        orbit_m.open("TestRing-trackOrbit.dat");
        try {
            builder_m.BuildMagnets();
        } catch (std::exception& exc) {
            std::cerr << "Exception " << exc.what() << std::endl;
        } catch (ClassicException& exc) {
            std::cerr << "Exception " << exc.what() << std::endl;
            std::cerr << "          " << exc.where() << std::endl;
        }

        tracker_m.setAbsoluteError(1e-9);
        tracker_m.setRelativeError(1e-9);
        tracker_m.setMaximumStepNumber(maxSteps_m);
        tracker_m.setCharge(1.);
    }

    void TearDown( ) {
    }

    ~VerticalFFAMagnetTrackingTest() {
    }

    std::vector<double> loadDist(std::string filename);
    void printTrackOrbitHeader();
    void printTrackOrbitLine(std::vector<double> u);
    void printProbe(std::string probename, std::vector<double> u);
    double stepSize(std::vector<double> u, std::vector<double> v, double targetPhi);
    double getTstep(std::vector<double> u);

protected:
    BuildRing builder_m;
    BuildLine line_m;
    Tracking tracker_m;
    double zstep_m = 0.1; // mm
    std::ofstream orbit_m;
    const double p_mass_m = 938.2720813;
    int maxSteps_m = 100000;
};

std::vector<double> VerticalFFAMagnetTrackingTest::loadDist(std::string filename) {
    std::ifstream fin(filename.c_str());
    int n_events;
    double r, s, z, rp, sp, zp;
    fin >> n_events;
    fin >> r >> rp >> s >> sp >> z >> zp;
    double ke = 400.;
    double pz = ::sqrt((ke+p_mass_m)*(ke+p_mass_m)-p_mass_m*p_mass_m);
    std::vector<double> u(8, 0.);
    u[1] = s*1000.;
    u[2] = -r*1000.;
    u[3] = z*1000.;
    u[5] = sp*p_mass_m+pz;
    u[6] = -rp*p_mass_m;
    u[7] = zp*p_mass_m;
    u[4] = ::sqrt(u[5]*u[5]+u[6]*u[6]+u[7]*u[7]+p_mass_m*p_mass_m);
    std::cerr << "Firing particle with energy " <<  u[4]-p_mass_m << std::endl;
    return u;
}

//# The six-dimensional phase space data in the global Cartesian coordinates"
//# Part. ID    x [m]       beta_x*gamma       y [m]      beta_y*gamma        z [m]      beta_z*gamma"
//ID0 8.70878359e-16 -9.85553229e-01 1.42225229e+01 -2.51029270e-01 6.73339960e-02 -6.73375511e-03
//ID0 -9.68371329e-04 -9.85553398e-01 1.42222763e+01 -2.51028687e-01 6.73273811e-02 -6.73073732e-03

void VerticalFFAMagnetTrackingTest::printTrackOrbitHeader() {
    orbit_m << std::fixed << std::setprecision(15);
    orbit_m << "# The six-dimensional phase space data in the global Cartesian coordinates\n";
    orbit_m << "# Part. ID    x [m]       beta_x*gamma       y [m]      beta_y*gamma        z [m]      beta_z*gamma\n";
}

void VerticalFFAMagnetTrackingTest::printTrackOrbitLine(std::vector<double> u) {
    orbit_m << "ID0 " << u[1]/1000. << " " << u[5]/p_mass_m << " "
                      << u[2]/1000. << " " << u[6]/p_mass_m << " " 
                      << u[3]/1000. << " " << u[7]/p_mass_m << "\n";
}

//# Element RINGPROBE01 x (mm),  y (mm),  z (mm),  px ( ),  py ( ),  pz ( ), id,  turn,  time (ns) 
//RINGPROBE01   8.70876e-16   14.2225   0.0673368   -0.985545   -0.251063   -0.0067355   0   1   -9.98273e-31 
//RINGPROBE01   8.70876e-16   14.2225   0.0673368   -0.985545   -0.251063   -0.0067355   0   1   -9.98273e-31 

void VerticalFFAMagnetTrackingTest::printProbe(std::string probename, std::vector<double> u) {
    std::ofstream fout(std::string(probename+".loss").c_str());
    fout << std::fixed << std::setprecision(15);
    fout << "# Element " << probename << " x (mm),  y (mm),  z (mm),  px ( ),  py ( ),  pz ( ), id,  turn,  time (ns) \n"
         << probename << " " << u[1]/1e3 << " " << u[2]/1e3 << " " << u[3]/1e3 << " " << u[5]/p_mass_m << " " << u[6]/p_mass_m << " " << u[7]/p_mass_m << " 0 1 " << u[0] << std::endl; 
}

// Return the time step
// if negative, we are straddling the output plane (don't update u)
double VerticalFFAMagnetTrackingTest::getTstep(std::vector<double> u) {
    double velocity = 300.*::sqrt(u[5]*u[5]+u[6]*u[6]+u[7]*u[7])/u[4];
    double tstep = ::fabs(zstep_m/velocity);
    return tstep;
}

double VerticalFFAMagnetTrackingTest::stepSize(std::vector<double> u, std::vector<double> v, double phiTarget) {
    double u_dphi = atan2(u[2], u[1])-phiTarget;
    double v_dphi = atan2(v[2], v[1])-phiTarget;
    //std::cerr << "step size ";
    double tstep;
    if (u_dphi/v_dphi < 0) {
        tstep = -::fabs(v[0]-u[0])/2;
        //std::cerr << "straddling  ";
    } else {
        tstep = getTstep(u);
        if (::fabs(v[0]-u[0]-tstep) > 1e-6) {
            tstep = ::fabs(v[0]-u[0]);
            //std::cerr << "approaching ";
        } else {
            //std::cerr << "tracking    ";
        }
    }
    //std::cerr << "  ut: " << u[0] << " vt: " << v[0] << " uphi: " << u_dphi << " vphi: " << v_dphi << " dt: "<< tstep << std::endl;
    //std::cerr << "  v:  " << v[1] << " " << v[2] << " " << v[3] << " " << v[4] << std::endl;
    //std::cerr << "  u:  " << u[1] << " " << u[2] << std::endl;
    return tstep;
}

TEST_F(VerticalFFAMagnetTrackingTest, TimeTrackingTest) {
    builder_m.MapRing();
    std::vector<double> u = loadDist("disttest.dat");
    Component* ring = dynamic_cast<Component*>(builder_m.getRing());
    printTrackOrbitHeader();
    printProbe("TESTPROBE01", u);
    std::vector<double> v = u;
    double targetPhi = (-1/4.+1./15)*2*Physics::pi;
    double tstep = getTstep(u);
    int stepNumber = 0;
    while (::fabs(tstep) > 1e-6 && stepNumber < maxSteps_m) {
        printTrackOrbitLine(v);
        try {
            tracker_m.integrate(Tracking::t, ::fabs(tstep), &v[0], ring, ::fabs(tstep));
        } catch (std::exception& exc) {
            std::cerr << "Exception " << exc.what() << std::endl;
            break;
        } catch (ClassicException& exc) {
            std::cerr << "Exception " << exc.what() << std::endl;
            std::cerr << "          " << exc.where() << std::endl;
            break;
        }
        tstep = stepSize(u, v, targetPhi);
        if (tstep > 0) {
            u = v;
        } else {
            v = u;
        }
        ++stepNumber;
    }
    printProbe("TESTPROBE02", u);
    printTrackOrbitLine(u);
}

TEST_F(VerticalFFAMagnetTrackingTest, ZTrackingTest) {
    std::vector<double> u = loadDist("disttest.dat");
    Component* line = dynamic_cast<Component*>(&line_m);
    printTrackOrbitHeader();
    printProbe("TESTPROBE01", u);
    u = line_m.transformToLocal(u);
    try {
        line_m.setIsFirstCell(true);
        u = line_m.transformToNextCell(u);
        tracker_m.integrate(Tracking::z, 3.0-u[3], &u[0], line, 1.);

        line_m.setIsFirstCell(false);
        u = line_m.transformToNextCell(u);
        tracker_m.integrate(Tracking::z, 3.0-u[3], &u[0], line, 1.);

    } catch (std::exception& exc) {
        std::cerr << "Exception " << exc.what() << std::endl;
    } catch (ClassicException& exc) {
        std::cerr << "Exception " << exc.what() << std::endl;
        std::cerr << "          " << exc.where() << std::endl;
    }
    u = line_m.transformToGlobal(u);
    printProbe("TESTPROBE02", u);
}


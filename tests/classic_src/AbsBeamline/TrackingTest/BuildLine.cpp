#include "Classic/AbsBeamline/EndFieldModel/Tanh.h"
#include "Physics/Physics.h"

#include "classic_src/AbsBeamline/TrackingTest/BuildLine.h"

bool BuildLine::apply(const Vector_t& R, const Vector_t& P, const double& t, 
                      Vector_t& E, Vector_t& B) {
    Vector_t tmpR;
    if (isFirstCell_m) {
        tmpR[0] = R[0]-offset_m;
        tmpR[1] = R[1];
        tmpR[2] = R[2]-cellLength_m/2.;
        firstCell_m.getFieldValue(tmpR, B);
    } else {
        tmpR[0] = R[0]+offset_m;
        tmpR[1] = R[1];
        tmpR[2] = R[2]-cellLength_m/2.;
        secondCell_m.getFieldValue(tmpR, B);
    }
    return false;
}

BuildLine::BuildLine() : Component("line"), firstCell_m("vffa1"), secondCell_m("vffa2") {
    using endfieldmodel::Tanh;
    double mm = 1000;
    Tanh* endfield1 = new Tanh(0.3*mm, 0.3*mm, 12);
    firstCell_m.setEndField(endfield1);
    firstCell_m.setMaxOrder(10);
    firstCell_m.setB0(-4.92);
    firstCell_m.setFieldIndex(0.877262);
    firstCell_m.setNegativeVerticalExtent(1.);
    firstCell_m.setPositiveVerticalExtent(2.);
    firstCell_m.setBBLength(8);
    firstCell_m.setWidth(4.);
    firstCell_m.initialise();

    Tanh* endfield2 = new Tanh(0.3*mm, 0.3*mm, 12);
    secondCell_m.setEndField(endfield2);
    secondCell_m.setMaxOrder(10);
    secondCell_m.setB0(-4.92/-3.);
    secondCell_m.setFieldIndex(0.877262);
    secondCell_m.setNegativeVerticalExtent(1.);
    secondCell_m.setPositiveVerticalExtent(2.);
    secondCell_m.setBBLength(8);
    secondCell_m.setWidth(4.);
    secondCell_m.initialise();

}


std::vector<double> BuildLine::transformToNextCell(std::vector<double> u) {
    double angle = 2.*Physics::pi/30;
    std::vector<double> v;
    v[0] = u[0]; //t
    v[1] = u[1]*cos(angle); // x
    v[2] = u[2]; // y
    v[3] = -u[1]*sin(angle); // z
    v[4] = u[4];
    v[5] = u[5]*cos(angle)+u[7]*sin(angle);
    v[6] = u[6];
    v[7] = -u[5]*sin(angle)+u[7]*cos(angle);

    return v;
}

std::vector<double> BuildLine::transformToGlobal(std::vector<double> u) {
    return u;
}

std::vector<double> BuildLine::transformToLocal(std::vector<double> u) {
    return u;
}

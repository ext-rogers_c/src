class Ring;

class BuildRing {
public:
    BuildRing();

    ~BuildRing();

    void BuildMagnets();

    void MapRing();

    Ring* getRing() {return ring_m;}
    
    size_t getNumberOfHalfCells() const {return numberOfHalfCells_m;}
private:

    void BuildHalfCell(double radialOffset, double field);
    Ring* ring_m;
    size_t numberOfHalfCells_m = 30;
    double bbLength_m = 4;
    
};
#include "BeamlineGeometry/StraightGeometry.h"
#include "Classic/AbsBeamline/Component.h"
#include "Classic/AbsBeamline/VerticalFFAMagnet.h"

class EMField;

#ifndef BUILDLINE_H
#define BUILDLINE_H

class BuildLine : public Component {
public:
    BuildLine();
    ~BuildLine() {}

    bool apply(const Vector_t& R, const Vector_t& P, const double& t, 
               Vector_t& E, Vector_t& B) override;

    StraightGeometry& getGeometry() {return geom_m;}
    const StraightGeometry& getGeometry() const {return geom_m;}
    virtual void accept(BeamlineVisitor &visitor) const {}
    virtual ElementBase* clone() const {BuildLine* line = NULL; return line;}
    virtual EMField& getField() {EMField* nofield = NULL; return *nofield;}
    virtual const EMField& getField() const {EMField* nofield = NULL; return *nofield;}
    virtual void initialise(PartBunchBase<double, 3> *bunch, double &startField, double &endField) {};
    virtual void getDimensions(double &zBegin, double &zEnd) const {}
    virtual bool bends() const {return true;}
    virtual void finalise() {}

    void setIsFirstCell(bool isFirstCell) {isFirstCell_m = isFirstCell;}
    std::vector<double> transformToNextCell(std::vector<double> u);
    std::vector<double> transformToGlobal(std::vector<double> u);
    std::vector<double> transformToLocal(std::vector<double> u);
private:
    StraightGeometry geom_m;
    VerticalFFAMagnet firstCell_m;
    VerticalFFAMagnet secondCell_m;
    bool isFirstCell_m;
    double cellLength_m;
    double offset_m;
};

#endif

#include "Physics/Physics.h"

#include "Classic/AbsBeamline/Ring.h"
#include "Classic/AbsBeamline/VerticalFFAMagnet.h"
#include "Classic/AbsBeamline/Offset.h"
#include "Classic/AbsBeamline/EndFieldModel/Tanh.h"
#include "BasicActions/DumpEMFields.h"
#include "classic_src/AbsBeamline/TrackingTest/BuildRing.h"


BuildRing::BuildRing() {
    ring_m = new Ring("Ring");
}

BuildRing::~BuildRing() {
    delete ring_m;
}

void BuildRing::BuildMagnets() {
    double dphi = -2*Physics::pi/numberOfHalfCells_m/2.;
    ring_m->setLatticeRInit(-14350.158);
    ring_m->setLatticeThetaInit(dphi);
    for (size_t i = 0; i < numberOfHalfCells_m; i += 2) { //
        BuildHalfCell(+0.347745, -4.92);
        BuildHalfCell(-0.347745, +1.64);
    }
    ring_m->setRingAperture(0.1, 100.0);
    ring_m->lockRing();

    Vector_t testR(1.53593, -14.459, 0), testP, testB, testE;
    ring_m->apply(testR, testP, 0., testE, testB);
    std::cerr << "Test field " << testR << " " << testB << std::endl;
}

void BuildRing::BuildHalfCell(double deltaR, double field) {
    /*
    using endfieldmodel::Tanh;
    const double mm = 1000.;
    double dphi = -2*Physics::pi/numberOfHalfCells_m;
    Offset toBeginning = Offset::localCartesianOffset("bb_offset",
                                       Vector_t(-bbLength_m/2., 0., 0.),
                                       Vector_t(1., 0., 0.));
    Offset quarterCell = Offset::localCartesianOffset("qrtr_cell_offset",
                                       Vector_t(3.0/2., 0., 0.),
                                       Vector_t(1., 0., 0.));
    Offset radialOffset = Offset::localCartesianOffset("radial_offset",
                                       Vector_t(0., deltaR, 0.),
                                       Vector_t(1., 0., 0.));
    Offset antiradialOffset = Offset::localCartesianOffset("antiradial_offset",
                                       Vector_t(0., -deltaR, 0.),
                                       Vector_t(1., 0., 0.));
    Offset cellBend = Offset::localCartesianOffset("cell_bend_offset",
                                       Vector_t(0., 0., 0.),
                                       Vector_t(cos(dphi), sin(dphi), 0.));
    VerticalFFAMagnet vffa("vffa");
    size_t maxOrder = 10;
    Tanh* endfield = new Tanh(0.3*mm, 0.3*mm, maxOrder+2);
    vffa.setEndField(endfield);
    vffa.setMaxOrder(maxOrder);
    vffa.setB0(field);
    vffa.setFieldIndex(0.877262);
    vffa.setNegativeVerticalExtent(1.);
    vffa.setPositiveVerticalExtent(2.);
    vffa.setBBLength(bbLength_m);
    vffa.setWidth(4.);
    vffa.initialise();
    ring_m->appendElement(quarterCell);
    ring_m->appendElement(radialOffset);
    ring_m->appendElement(toBeginning);
    ring_m->appendElement(vffa);
    ring_m->appendElement(toBeginning);
    ring_m->appendElement(antiradialOffset);
    ring_m->appendElement(quarterCell);
    ring_m->appendElement(cellBend);
    */
}

void BuildRing::MapRing() {
    DumpEMFields dump;
    dump.setCoordinateSystem(DumpEMFields::CARTESIAN);
    dump.setFilename("FieldMapXY.dat");
    std::vector<int> gridSize = {320, 320, 1, 1};
    std::vector<double> origin = {-16.0, -16.0, 0., 0.};
    std::vector<double> spacing = {0.1, 0.1, 0.1, 0.1};

    dump.setOrigin(origin);
    dump.setSpacing(spacing);
    dump.setGridSize(gridSize);
    dump.buildGrid();
    dump.addToDumpSet();
    DumpEMFields::writeFields(ring_m);
}

/*
 *  Copyright (c) 2014, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include "gtest/gtest.h"
#include <sstream>
#include <math.h>

#include "Classic/Utilities/GeneralClassicException.h"
#include "Classic/AbsBeamline/EndFieldModel/Arctan.h"

TEST(ArctanTest, testConstructor) {
    endfieldmodel::Arctan arctan(2, 0.001); // M, L
    EXPECT_NEAR(arctan.function(0, 0), 1.0, 1e-3);
    EXPECT_NEAR(arctan.function(1.0, 0), 0.5, 1e-3);
    EXPECT_NEAR(arctan.function(0.99, 0), 0.968, 1e-3);
}

TEST(ArctanTest, testClone) {
    endfieldmodel::Arctan arctan(2, 0.001); // M, L
    endfieldmodel::Arctan* arctan2 =
                        dynamic_cast<endfieldmodel::Arctan*>(arctan.clone());
    EXPECT_NEAR(arctan2->function(0, 0), arctan.function(0, 0), 1e-12);
    EXPECT_NEAR(arctan2->function(1.0, 0), arctan.function(1.0, 0), 1e-12);
    EXPECT_NEAR(arctan2->function(0.99, 0), arctan.function(0.99, 0), 1e-12);
}


TEST(ArctanTest, testPrint) {
    endfieldmodel::Arctan arctan(2, 0.001); // M, L
    std::stringstream strstr;
    arctan.print(strstr);
    EXPECT_EQ(strstr.str(), "Arctan(M=2 L=0.001)");

}

TEST(ArctanTest, testAtanFunction) {
    endfieldmodel::Arctan arctan(0, 0.0); // atanFunction does not use M, L
    for (double x = 0.0; x < 10.1; x += 1.0) {
        for (int n = 1; n < 5; ++n) {
            double dx = 1e-6;
            double numerical = (arctan.atanFunction(x+dx, n-1)-arctan.atanFunction(x-dx, n-1))/2/dx;
            double analytical = arctan.atanFunction(x, n);
            EXPECT_NEAR(numerical, analytical, 1e-6) << "Failed for x " << x << " n " << n;
        }
    }
}


TEST(ArctanTest, testAtanFunctionThrows) {
    endfieldmodel::Arctan arctan(0, 0.0); // atanFunction does not use M, L
    EXPECT_NO_THROW(arctan.atanFunction(0.0, 13));
    EXPECT_THROW(arctan.atanFunction(0.0, 14), GeneralClassicException);
}

TEST(ArctanTest, testFunction) {
    endfieldmodel::Arctan arctan(0.5, 0.15); // M, L
    for (double x = -2.0; x < 2.1; x += 0.01) {
        for (int n = 1; n < 8; ++n) {
            double dx = 1e-6;
            double numerical = (arctan.function(x+dx, n-1)-arctan.function(x-dx, n-1))/2/dx;
            double analytical = arctan.function(x, n);           
            double tolerance = std::fabs(analytical*1e-4);
            if (tolerance < 1e-4) {
                tolerance = 1e-4;
            }
            EXPECT_NEAR(numerical, analytical, tolerance)
                 << "x: " << x << " n: " << n 
                 << " numerical: " << numerical
                 << " analytical: " << analytical 
                 << " tolerance: " << tolerance << std::endl;
        }
    }
}

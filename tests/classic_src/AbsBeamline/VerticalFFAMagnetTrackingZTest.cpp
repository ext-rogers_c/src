#include <iomanip>

#include "gtest/gtest.h"

#include "classic_src/AbsBeamline/TrackingTest/Tracking.h"
#include "classic_src/AbsBeamline/TrackingTest/BuildLine.h"

class VerticalFFAMagnetTrackingZTest : public ::testing::Test {
public:
    VerticalFFAMagnetTrackingZTest() {
    }

    void SetUp( ) {
        orbit_m.open("TestRing-trackOrbit.dat");
        tracker_m.setAbsoluteError(1e-9);
        tracker_m.setRelativeError(1e-9);
        tracker_m.setMaximumStepNumber(maxSteps_m);
        tracker_m.setCharge(1.);
    }

    void TearDown( ) {
    }

    ~VerticalFFAMagnetTrackingZTest() {
    }

    std::vector<double> loadDist(std::string filename);
    void printTrackOrbitHeader();
    void printTrackOrbitLine(std::vector<double> u);
    void printProbe(std::string probename, std::vector<double> u);
    double stepSize(std::vector<double> u, std::vector<double> v, double targetPhi);
    double getTstep(std::vector<double> u);

protected:
    BuildLine line_m;
    Tracking tracker_m;
    double zstep_m = 1.0; // mm
    std::ofstream orbit_m;
    const double p_mass_m = 938.2720813;
    int maxSteps_m = 100000;
};


TEST_F(VerticalFFAMagnetTrackingZTest, TrackingTest) {

}

#include <gtest/gtest.h>
#include "opal_test_utilities/SilenceTest.h"
#include "AbsBeamline/Probe.h"
#include "BeamlineCore/ProbeRep.h"
#include "Algorithms/PartBunch.h"
#include "Algorithms/PartData.h"
#include "Algorithms/OpalParticle.h"


class ProbeTest : public ::testing::Test {
  public:
    ProbeTest() {};
    
    Vector_t pos;
    Vector_t mom;
    Vector_t bfield;
    Vector_t efield;
    double dt;
    Vector_t pos_ref;
    Vector_t mom_ref;
    double t_ref;
    double tolerance;
    double xmin, xmax, ymin, ymax;
    Probe* probe;
    PartBunch* bunch;
    PartData* ref;
    
    void setup();
    void check(Vector_t x, Vector_t p, double t);
    void clear();
    OpalTestUtilities::SilenceTest silencer;
};

void ProbeTest::setup() {
    probe = new ProbeRep("aname");
    ref = new PartData(1, 938.2720813*1e6, 3*1e6);
    bunch = new PartBunch(ref);
    bunch->push_back(OpalParticle(0, 0, 0, 0, 0, 0));
    bunch->R[0] = pos;
    bunch->P[0] = mom;
    bunch->Bf[0] = bfield;
    bunch->Ef[0] = efield;
    probe->setDimensions(xmin, xmax, ymin, ymax);
}

void ProbeTest::check(Vector_t x, Vector_t p, double t) {
    for (size_t i = 0; i < 3; ++i) {
        EXPECT_NEAR(x[i], pos_ref[i], tolerance); 
        EXPECT_NEAR(p[i], mom_ref[i], tolerance); 
    }

    EXPECT_NEAR(t, t_ref, tolerance);
}

void ProbeTest::clear() {
    delete ref;
    delete bunch;
    delete probe;
}

TEST_F(ProbeTest, doOneCheckTest) {
    pos = Vector_t(0., 2., 3.);
    mom = Vector_t(4., 5., 6.);
    xmin = 0.;
    ymin = 1.;
    xmax = 0.;
    ymax = 10.;
    dt = 0.;
    pos_ref = pos;
    mom_ref = mom;
    t_ref = 0.;
    tolerance = 1e-12;
    setup();
    //probe->doSetGeom(); // ACK! fails.
    probe->doOneCheck(bunch, dt, 0);
    check(probe->probePoint_m, probe->probeMom_m, probe->probeT_m);
    clear();
}

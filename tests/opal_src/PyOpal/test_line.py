import PyOpal.vertical_ffa_magnet
import PyOpal.opal_element
import PyOpal.line

myline = PyOpal.line.Line()
print(myline)
magnet1 = PyOpal.vertical_ffa_magnet.VerticalFFAMagnet()
magnet2 = PyOpal.vertical_ffa_magnet.VerticalFFAMagnet()
print(magnet1, magnet2)
myline.append(magnet1)
print("Line of length", len(myline))
myline.append(magnet2)
print("Line of length", len(myline))
print(myline[0], myline[1])
myline[1] = magnet2
print("Line of length", len(myline))
print(myline[0], myline[1])
myline.append(myline)


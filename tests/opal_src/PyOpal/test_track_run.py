import os

import PyOpal.field
import PyOpal.parser
import PyOpal.track
import PyOpal.track_run
import PyOpal.beam
import PyOpal.field_solver
import PyOpal.opal_element # BUG - this import needs to have been done other wise PyOpal.line doesn't work.
import PyOpal.line
import PyOpal.distribution
import PyOpal.ring_definition
import PyOpal.vertical_ffa_magnet
import PyOpal.local_cartesian_offset
import matplotlib.pyplot

def make_field_solver():
    field_solver = PyOpal.field_solver.FieldSolver()
    field_solver.field_solver_type = "NONE"
    field_solver.mesh_size_x = 5
    field_solver.mesh_size_y = 5
    field_solver.mesh_size_t = 5
    field_solver.parallelize_x = False
    field_solver.parallelize_y = False
    field_solver.parallelize_t = False
    field_solver.boundary_x = "open"
    field_solver.boundary_y = "open"
    field_solver.boundary_t = "open"
    field_solver.bounding_box_increase = 2

    field_solver.register()
    return field_solver

def make_magnet():
    magnet1 = PyOpal.vertical_ffa_magnet.VerticalFFAMagnet()
    magnet1.b0 = 1.0
    magnet1.field_index = 1.31
    magnet1.max_horizontal_power = 12
    magnet1.centre_length = 0.5
    magnet1.end_length = 0.15
    magnet1.end_field_model = "arctan"
    magnet1.width = 0.5
    magnet1.height_neg_extent = 1.0
    magnet1.height_pos_extent = 1.0
    magnet1.bb_length = 12.0
    magnet1.enge_parameters = [1.0, 2.0, 3.0]
    magnet1.bb_start_position = [0.0, 0.0, 0.0]
    magnet1.bb_start_normal = [0.0, 0.0, 1.0]
    magnet1.bb_end_position = [0.0, 0.0, 12.0]
    magnet1.bb_end_normal = [0.5, 0.0, 1.0]
    return magnet1

def make_line():
    myline = PyOpal.line.Line()
    ring = PyOpal.ring_definition.RingDefinition()
    ring.lattice_initial_r = 4.0
    ring.beam_initial_r = 0.0
    ring.minimum_r = 0.5
    ring.maximum_r = 10.0
    ring.is_closed = "FALSE"
    magnet1 = make_magnet()
    offset = PyOpal.local_cartesian_offset.LocalCartesianOffset()
    offset.end_position_x = 0.0
    offset.end_position_y = 1.0
    offset.normal_x = 1.0
    magnet2 = make_magnet()

    myline.append(ring)
    myline.append(magnet1)
    myline.append(offset)
    myline.append(magnet2)
    myline.register()
    return myline


print("Running track_run test")
beam = PyOpal.beam.Beam()
beam.mass = 0.938272
beam.charge = 1.0
beam.momentum = 0.1
beam.beam_frequency = 1.0
beam.number_of_slices = 10
beam.register()

distribution = PyOpal.distribution.Distribution()
distribution.type = "FROMFILE"
root_dir = os.environ["OPAL_BUILD_PATH"]
print(root_dir)
distribution.fname = os.path.join(root_dir,"tests/opal_src/PyOpal/dummy_lattice/disttest2.dat")
distribution.register()

line = make_line()
field_solver = make_field_solver()

track = PyOpal.track.Track()
track.line = "LINE"
track.beam = "BEAM"
run = PyOpal.track_run.TrackRun()
run.method = "CYCLOTRON-T"
run.keep_alive = True
print("set beam")
run.beam_name = "BEAM"
PyOpal.parser.list_objects()
print("get beam")
print(track.beam)
print("done")
print("TRACK")
track.execute()
print("TRACK RUN")
run.execute()

print("line[0].get_field_value:")
field0 = line[0].get_field_value(0.0, 1.0, 0.0, 0.0)
print("PyOpal.field.get_field_value:")
field1 = PyOpal.field.get_field_value(0.0, 1.0, 0.0, 0.0)
print("line[1].get_field_value:")
field2 = line[1].get_field_value(0.0, 0.0, 0.0, 0.0)

print("ring", field0)
print("global field", field1)
print("vffa        ", field2)




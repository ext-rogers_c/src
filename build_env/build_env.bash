#!/bin/bash

source ${OPAL_BUILD_PATH}/build_env/make_lib.bash

export opal_external_dir="/home/cr67/OPAL/external_rogers_mint"
export opal_external_source_dir="${opal_external_dir}/source"
export opal_external_build_dir="${opal_external_dir}/build"
export opal_external_install_dir="${opal_external_dir}/install"
export opal_external_build_log="${opal_external_dir}/external_build.log"
export opal_external_install_dir="${opal_external_dir}/install"

o_install=${opal_external_install_dir}

export PATH=${o_install}/bin/:${PATH}
export CPPFLAGS="-I${o_install}/include"
export LDFLAGS="-L${o_install}/lib64 -L${o_install}/lib"
export LD_LIBRARY_PATH="${o_install}/lib64:${o_install}/lib:/usr/lib/x86_64-linux-gnu/"
export PKG_CONFIG_PATH="${o_install}/lib64/pkgconfig/:${o_install}/lib/pkgconfig/"

mkdir -p ${opal_external_source_dir}
mkdir -p ${opal_external_build_dir}
mkdir -p ${opal_external_install_dir}


function build_gcc() {
    get_lib "https://gmplib.org/download/gmp/" "gmp" "-6.1.2" ".tar.bz2" \
            --disable-shared --enable-static

    get_lib "https://www.mpfr.org/mpfr-current/" "mpfr" "-4.0.2" ".tar.gz" \
            --disable-shared --enable-static --with-gmp=${o_install}

    get_lib "https://ftp.gnu.org/gnu/mpc/" "mpc" "-1.1.0" ".tar.gz" \
            --disable-shared --enable-static --with-gmp=${o_install} \
            --with-mpfr=${o_install}

    get_lib "ftp://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/gcc-7.4.0/" \
            "gcc" "-7.4.0" ".tar.gz" \
            --with-gmp=${o_install} --with-mpfr=${o_install} --with-mpc=${o_install} \
            --enable-shared --enable-threads=posix \
            --disable-libada --disable-multilib

}

function build_support() {
    echo "Building support libs using gcc version"
    echo `gcc --version`
    get_lib "https://sourceforge.net/projects/libpng/files/zlib/1.2.11/" \
            "zlib" "-1.2.11" ".tar.gz"

    # note https://blah/v1.10/ trailing / causes wget to fail
    get_lib "https://download.open-mpi.org/release/open-mpi/v1.10" \
            "openmpi" "-1.10.7" ".tar.gz" \
            --disable-mpi-fortran \
            --disable-dlopen

    CONFIG_NAME=config \
    get_lib "https://www.openssl.org/source/" \
            "openssl" "-1.1.1d" ".tar.gz"

    get_lib "http://ftp.snt.utwente.nl/pub/software/gnu/gsl/" \
            "gsl" "-2.5" ".tar.gz"

    get_lib "https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8/hdf5-1.8.21/src/" \
            "hdf5" "-1.8.21" ".tar.gz"

    get_lib "amas.web.psi.ch/Downloads/H5hut/" "H5hut" "-2.0.0rc3" ".tar.gz" \
            --enable-shared

    CONFIG_NAME=bootstrap \
    get_lib "https://github.com/Kitware/CMake/releases/download/v3.15.3/" \
            "cmake" "-3.15.3" ".tar.gz"

    get_file "https://github.com/google/googletest/archive/" \
            "release" "-1.10.0" ".tar.gz"
    cmake_configure_lib "googletest-release" "-1.10.0" "-DBUILD_SHARED_LIBS=ON"
    make_lib "googletest-release" "-1.10.0"

    echo "Finished building support libs"
    echo
    echo
}

function build_geant4() {
    #get_file "http://geant4-data.web.cern.ch/geant4-data/releases/geant4.10.06.p03.tar.gz" \
    #        "geant4" "-10.6" ".tar.gz"
    cmake_configure_lib "geant4" ".10.06.p03" "-DBUILD_SHARED_LIBS=ON"
    make_lib "geant4" ".10.06.p03"
}

function build_python() {
    #get_lib "ftp://sourceware.org/pub/libffi/" "libffi" "-3.2.1" ".tar.gz"

    # I had trouble linking to ssl and ffi libraries; so hacked it. Some of this
    # stuff is probably unnecessary
    #get_lib "https://www.python.org/ftp/python/3.7.4/" \
    #        "Python" "-3.7.4" ".tgz" \
    #        --enable-shared \
    #        --with-openssl=${o_install} \
    #        --with-system-ffi=${o_install} \
    #        PYTHONPATH="${o_install}/lib64:${o_install}/lib"

    get_pip "https://bootstrap.pypa.io"
    ${o_install}/bin/pip3 install numpy matplotlib h5py scipy
}

function build_boost() {
    get_boost_lib "https://dl.bintray.com/boostorg/release/1.68.0/source/" \
                  "boost" "_1_68_0" ".tar.gz"
}

function build_root() {
    get_root_lib "https://root.cern/download/" "root" "6.18.04" ".source.tar.gz" \
        -DCMAKE_CXX_COMPILER=${o_install}/bin/g++ \
        -DCMAKE_C_COMPILER=${o_install}/bin/gcc \
        -DCMAKE_Fortran_COMPILER=${o_install}/bin/gfortran \
        -DPYTHON_EXECUTABLE=${o_install}/bin/python3 \
        -DVERBOSE=1
}

#build_gcc

# now we (maybe) have a working compiler...
export CC="${o_install}/bin/gcc"
export CXX="${o_install}/bin/g++"

#build_support
#build_python
# error ... python put header files in "python3.7m" (cf PEP 3149) which boost
# didn't see
#build_boost
build_geant4
#link python3 to python
#install xboa
#install platypus (ga optimisation code)
#install tkinter (python needs tk-dev)
#!/bin/bash

function get_file() {
    local package_location=$1
    local package_name=$2
    local package_vers=$3
    local suffix=$4

    local dirname="${package_name}"
    dirname+="${package_vers}"
    local filename="${dirname}${suffix}"
    local url="${package_location}/${filename}"

    cd ${opal_external_source_dir}
    echo "    Getting file from ${url}"

    if [ -e ${filename} ]; then
        rm ${filename}
    fi
    wget --no-check-certificate ${url} -O ${filename} > ${opal_external_build_log} 2>&1
    if [ $? -ne 0 ]; then
        echo "Failed to wget '${url}' $?"
        rm ${filename}
        exit 1;
    fi
    if [ -z ${suffix} ]; then
        echo "No suffix; not unpacking ${package_name}"
        return;
    fi

    echo "    Extracting ${filename}"
    if [ -e ${dirname} ]; then
        rm -rf ${dirname}
    fi
    tar -xf $filename > ${opal_external_build_log} 2>&1
    if [ $? -ne 0 ]; then
        echo "Failed to extract '${filename}'"
        exit 2;
    fi
}

CONFIG_NAME=configure
PREFIX_NAME=--prefix
function configure_lib() {
    local package_name=$1
    local package_vers=$2
    local configure_arr=($@)
    configure_arr=("${configure_arr[@]:2}")
    local dirname="${package_name}${package_vers}"
    if [ -e ${opal_external_build_dir}/${dirname} ]; then
        rm -rf ${opal_external_build_dir}/${dirname}
    fi
    mkdir -p ${opal_external_build_dir}/${dirname}
    cd ${opal_external_build_dir}/${dirname}
    echo "    ${CONFIG_NAME} ${dirname} with extra options ${configure_arr[@]}"
    ${opal_external_source_dir}/${dirname}/${CONFIG_NAME} \
                 ${PREFIX_NAME}=${opal_external_install_dir} \
                 ${configure_arr[@]} > ${opal_external_build_log} 2>&1
    if [ $? -ne 0 ]; then
        echo "Failed to configure '${dirname}'"
        cd ..
        #rm -r ${opal_external_build_dir}/${dirname}
        exit 1
    fi
}

CMAKE_NAME=cmake
function cmake_configure_lib() {
    local package_name=$1
    local package_vers=$2
    local configure_arr=($@)
    configure_arr=("${configure_arr[@]:2}")
    local dirname="${package_name}${package_vers}"
    if [ -e ${opal_external_build_dir}/${dirname} ]; then
        rm -rf ${opal_external_build_dir}/${dirname}
    fi
    mkdir -p ${opal_external_build_dir}/${dirname}
    cd ${opal_external_build_dir}/${dirname}
    echo "    Cmake configuring ${dirname} with extra options ${configure_arr[@]}"
    ${CMAKE_NAME} ${opal_external_source_dir}/${dirname} \
          -DCMAKE_INSTALL_PREFIX=${opal_external_install_dir} \
          ${configure_arr[@]} > ${opal_external_build_log} 2>&1
    if [ $? -ne 0 ]; then
        echo "Failed to configure '${dirname}'"
        cd ..
        #rm -r ${opal_external_build_dir}/${dirname}
        exit 1
    fi
}


function make_lib() {
    local package_name=$1
    local package_vers=$2
    local dirname="${package_name}${package_vers}"
    cd ${opal_external_build_dir}/${dirname}
    echo "    Making ${dirname}"
    make > ${opal_external_build_log} 2>&1
    if [ $? -ne 0 ]; then
        echo "Failed to make '${dirname}'"
        exit 1
    fi
    echo "    Installing ${dirname}"
    make install > ${opal_external_build_log} 2>&1
    if [ $? -ne 0 ]; then
        echo "Failed to install '${dirname}'"
        exit 2
    fi
}

function get_lib() {
    local package_location=$1
    local package_name=$2
    local package_vers=$3
    local suffix=$4
    local configure_arr=($@)
    configure_arr=("${configure_arr[@]:4}")
    echo "Doing ${package_name}"
    get_file "${package_location}" "${package_name}" "${package_vers}" "${suffix}"
    configure_lib "${package_name}" "${package_vers}"  ${configure_arr[@]}
    make_lib "${package_name}" "${package_vers}"
    echo "Done ${package_name}"
    echo
}

function get_cmake_lib() {
    local package_location=$1
    local package_name=$2
    local package_vers=$3
    local suffix=$4
    local configure_arr=($@)
    configure_arr=("${configure_arr[@]:4}")
    echo "Doing ${package_name}"
    get_file "${package_location}" "${package_name}" "${package_vers}" "${suffix}"
    cmake_configure_lib "${package_name}" "${package_vers}"  ${configure_arr[@]}
    make_lib "${package_name}" "${package_vers}"
    echo "Done ${package_name}"
    echo
}

function get_boost_lib() {
    echo "Doing boost"
    local package_location=$1
    local package_name=$2
    local package_vers=$3
    local suffix=$4
    local dirname="${package_name}${package_vers}"
    get_file "${package_location}" "${package_name}" "${package_vers}" "${suffix}"
    echo "    Bootstrapping ${dirname}"
    cd ${opal_external_source_dir}/${dirname}
    ./bootstrap.sh --prefix=${o_install} \
        --with-python-root=${o_install}  > ${opal_external_build_log} 2>&1
    if [ $? -ne 0 ]; then
        echo "Failed to build boost"
        cd ..
        #rm -r ${opal_external_source_dir}/${dirname}
        exit 1
    fi
    echo "    Installing ${dirname}"
    ./b2 install > ${opal_external_build_log} 2>&1
    if [ $? -ne 0 ]; then
        echo "Failed to build boost"
        cd ..
        #rm -r ${opal_external_source_dir}/${dirname}
        exit 1
    fi
    echo "Done boost"
}


function get_root_lib() {
    local package_location=$1
    local package_name=$2
    local package_vers=$3
    local suffix=$4
    local configure_arr=($@)
    configure_arr=("${configure_arr[@]:4}")
    echo "Doing ${package_name} with path"
    echo "    ${PATH}"
    local dirname="${package_name}-${package_vers}"
    #get_file "${package_location}" "${package_name}" "_v${package_vers}" "${suffix}"
    cd ${opal_external_source_dir}/${dirname}
    cmake_configure_lib "${package_name}" "-${package_vers}"  ${configure_arr[@]}
    make_lib "${package_name}" "-${package_vers}"
    echo "Done ${package_name}"
    echo
}

function get_pip() {
    echo "Getting pip abc"
    local package_location=$1
    local package_name="get-pip"
    cd ${opal_external_source_dir}
    if [ -e ${package_name} ]; then
        rm -r ${package_name}
    fi
    mkdir -p ${package_name}
    cd ${package_name}
    echo "    Getting pip from ${package_location}/${package_name}.py"
    curl ${package_location}/${package_name}.py -O ${package_name}.py
    python3 ${package_name}.py
}

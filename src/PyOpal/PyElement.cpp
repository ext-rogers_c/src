#include "PyOpal/PyElement.h"

//using namespace boost::python;
namespace PyOpal {
namespace PyElementNS {

std::map<AttributeType, std::string> attributeName = std::map<AttributeType, std::string>({
    {DOUBLE, "double"}
});

} // PyElementNS
} // PyOpal
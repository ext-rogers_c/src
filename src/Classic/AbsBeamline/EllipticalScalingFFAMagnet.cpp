
#include <cmath>

#include "Utilities/GeneralClassicException.h"
#include "Algorithms/PartBunch.h"
#include "AbsBeamline/BeamlineVisitor.h"
#include "AbsBeamline/EllipticalScalingFFAMagnet.h"

EllipticalScalingFFAMagnet::EllipticalScalingFFAMagnet(const std::string &name)
        : Component(name),
         planarArcGeometry_m(0., 1.), dummy(), endField_m(NULL) {
    setElType(isDrift);
}

EllipticalScalingFFAMagnet::EllipticalScalingFFAMagnet(const EllipticalScalingFFAMagnet &right)
        : Component(right),
          planarArcGeometry_m(right.planarArcGeometry_m),
          dummy(), maxOrder_m(right.maxOrder_m), tanDelta_m(right.tanDelta_m),
          k_m(right.k_m), Bz_m(right.Bz_m), mu0_m(right.mu0_m),
          a_m(right.a_m), muMin_m(right.muMin_m), muMax_m(right.muMax_m),
          phiStart_m(right.phiStart_m),
          phiEnd_m(right.phiEnd_m), azimuthalExtent_m(right.azimuthalExtent_m),
          verticalExtent_m(right.verticalExtent_m), centre_m(right.centre_m),
          dfCoefficients_m(right.dfCoefficients_m) {
    if (endField_m != NULL) {
        delete endField_m;
    }
    endField_m = right.endField_m->clone();
    RefPartBunch_m = right.RefPartBunch_m;
    setElType(isDrift);
    Bz_m = right.Bz_m;
}

EllipticalScalingFFAMagnet::~EllipticalScalingFFAMagnet() {
    if (endField_m != NULL) {
        delete endField_m;
    }
}

ElementBase* EllipticalScalingFFAMagnet::clone() const {
    EllipticalScalingFFAMagnet* magnet = new EllipticalScalingFFAMagnet(*this);
    magnet->initialise();
    return magnet;
}

EMField &EllipticalScalingFFAMagnet::getField() {
    return dummy;
}

const EMField &EllipticalScalingFFAMagnet::getField() const {
    return dummy;
}

bool EllipticalScalingFFAMagnet::apply(const size_t &i, const double &t,
                    Vector_t &E, Vector_t &B) {
    return apply(RefPartBunch_m->R[i], RefPartBunch_m->P[i], t, E, B);
}

void EllipticalScalingFFAMagnet::initialise() {
    calculateDfCoefficients();
    double endX, endY;
    convertToCartesian(mu0_m, phiEnd_m, endX, endY);
    planarArcGeometry_m.setElementLength(0.); // length = phi r
    planarArcGeometry_m.setCurvature(1.);
    if (muMin_m < 0.) {
        throw GeneralClassicException("EllipticalScalingFFAMagnet::initialise()",
        "Inner edge of Elliptical Scaling Magnet has mu < 0 which is non-physical");
    }
}

void EllipticalScalingFFAMagnet::initialise(PartBunchBase<double, 3> *bunch, double &startField, double &endField) {
    RefPartBunch_m = bunch;
    initialise();
}

void EllipticalScalingFFAMagnet::finalise() {
    RefPartBunch_m = NULL;
}

bool EllipticalScalingFFAMagnet::bends() const {
    return true;
}

BGeometryBase& EllipticalScalingFFAMagnet::getGeometry() {
    return planarArcGeometry_m;
}

const BGeometryBase& EllipticalScalingFFAMagnet::getGeometry() const {
    return planarArcGeometry_m;
}

void EllipticalScalingFFAMagnet::accept(BeamlineVisitor& visitor) const {
    visitor.visitEllipticalScalingFFAMagnet(*this);
}

bool EllipticalScalingFFAMagnet::getFieldValue(const Vector_t &R, Vector_t &B) const {
    Vector_t pos = R - centre_m;
    Vector_t posCyl(0., pos[1], 0.);
    convertToElliptical(pos[0], pos[2], posCyl[0], posCyl[2]);
    Vector_t bCyl(0., 0., 0.); //br bz bphi
    bool outOfBounds = getFieldValueElliptical(posCyl, bCyl);
    // convert back
    B[0] = 0.;
    B[1] = bCyl[1];
    B[2] = 0.;
    //std::cerr << "        getFieldValue pos: " 
    //          << std::setw(12) << pos << " posCyl: " 
    //          << std::setw(12) << posCyl << " B: " 
    //          << std::setw(12) << B << std::endl;
    return outOfBounds;

}


bool EllipticalScalingFFAMagnet::getFieldValueElliptical(const Vector_t &pos, Vector_t &B) const {

    double mu = pos[0];
    double z = 0.; //pos[1];
    double nu = pos[2];
   // std::cerr << "    getFieldValueElliptical.pos " << pos << std::endl;
    if (mu < muMin_m || mu > muMax_m) {
        return true;
    }
    if (nu > phiEnd_m) { // nu should be defined in range 0 < 2pi
        return true;
    }
    if (z < -verticalExtent_m || z > verticalExtent_m) {
        return true;
    }
    B[0] = 0.;
    B[1] = endField_m->function(nu - phiStart_m, 0)*Bz_m*pow(mu/mu0_m, k_m);
    B[2] = 0.;
    //std::cerr << "    " << endField_m->function(nu - phiStart_m, 0)
    //          << " " << Bz_m
    //          << " mu0 " << mu0_m << " ratio " << mu/mu0_m
    //          << " " << pow(mu/mu0_m, k_m) << std::endl;
    return false;
}


void EllipticalScalingFFAMagnet::calculateDfCoefficients() {
    dfCoefficients_m = std::vector<std::vector<double> >(maxOrder_m+1);
    dfCoefficients_m[0] = std::vector<double>(1, 1.); // f_0 = 1.*0th derivative
    for (size_t n = 0; n < maxOrder_m; n += 2) { // n indexes the power in z
        dfCoefficients_m[n+1] = std::vector<double>(dfCoefficients_m[n].size()+1, 0);
        for (size_t i = 0; i < dfCoefficients_m[n].size(); ++i) { // i indexes the derivative
            dfCoefficients_m[n+1][i+1] = dfCoefficients_m[n][i]/(n+1);
        }
        if (n+1 == maxOrder_m) {
            break;
        }
        dfCoefficients_m[n+2] = std::vector<double>(dfCoefficients_m[n].size()+2, 0);
        for(size_t i = 0; i < dfCoefficients_m[n].size(); ++i) { // i indexes the derivative
            dfCoefficients_m[n+2][i] = -(k_m-n)*(k_m-n)/(n+1)*dfCoefficients_m[n][i]/(n+2);
        }
        for(size_t i = 0; i < dfCoefficients_m[n+1].size(); ++i) { // i indexes the derivative
            dfCoefficients_m[n+2][i] += 2*(k_m-n)*tanDelta_m*dfCoefficients_m[n+1][i]/(n+2);
            dfCoefficients_m[n+2][i+1] -= (1+tanDelta_m*tanDelta_m)*dfCoefficients_m[n+1][i]/(n+2);
        }
    }

}

void EllipticalScalingFFAMagnet::setEndField(endfieldmodel::EndFieldModel* endField) {
    if (endField_m != NULL) {
        delete endField_m;
    }
    endField_m = endField;
}

void EllipticalScalingFFAMagnet::convertToElliptical(const double& x,
                                                     const double& y, 
                                                     double& mu, 
                                                     double& nu) const {
    // x = a cosh mu cos nu
    // y = a sinh mu sin nu
    // follow Sun, Mathematical Modelling and Applications, 2017
    // nu is defined in domain 0:2pi to make bounding box easier/cheaper
    double B = x*x+y*y-a_m*a_m;
    double quad_arg = ::sqrt(B*B+4*a_m*a_m*y*y);
    double p = (-B+quad_arg)/2/a_m/a_m;
    double q = (-B-quad_arg)/2/a_m/a_m;
    mu = 0.5*log(1-2*q+2*::sqrt(q*q-q));
    nu = asin(::sqrt(p));
    if (x < 0 && y >= 0) {
        nu = Physics::pi-nu;
    } else if (x <= 0 && y < 0) {
        nu = Physics::pi+nu;
    } else if (x > 0 && y < 0) {
        nu = 2.*Physics::pi-nu;
    } // x >= 0 and y >= 0 ... do nothing
    //std::cerr << "convertToElliptical nu: " << nu << " mu: " << mu 
    //          << " B: " << B << " quad_arg: " << quad_arg << " p: " << p << " q: " << q << " a_m: " << a_m << std::endl;
    //std::cerr << "convertToElliptical2 p: " << p << " sqrt p: " << ::sqrt(p) << " asin(sqrt(p))" << asin(::sqrt(p)) << std::endl; 
}

void EllipticalScalingFFAMagnet::convertToCartesian(const double& mu,
                                                    const double& nu, 
                                                    double& x, 
                                                    double& y) const {
    x = a_m*cosh(mu)*cos(nu);
    y = a_m*sinh(mu)*sin(nu);
}


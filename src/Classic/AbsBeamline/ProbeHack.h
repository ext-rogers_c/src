#ifndef CLASSIC_Probe_HH
#define CLASSIC_Probe_HH

#ifdef WITH_UNIT_TESTS
#include <gtest/gtest_prod.h>
#endif
    
#include "AbsBeamline/PluginElement.h"

#include <memory>
#include <string>

class PeakFinder;

// Class Probe
// ------------------------------------------------------------------------
/// Interface for probe.
//  Class Probe defines the abstract interface for a probe.

class Probe: public PluginElement {

public:
    /// Constructor with given name.
    explicit Probe(const std::string &name);

    Probe();
    Probe(const Probe &);
    void operator=(const Probe &) = delete;
    virtual ~Probe();

    /// Apply visitor to Probe.
    virtual void accept(BeamlineVisitor &) const override;

    /// Set probe histogram bin width
    void setStep(double step);
    ///@{ Member variable access
    virtual double getStep() const;
    ///@}
    virtual ElementBase::ElementType getType() const override;

private:
    /// Initialise peakfinder file
    virtual void doInitialise(PartBunchBase<double, 3> *bunch) override;
    /// Record probe hits when bunch particles pass
    virtual bool doCheck(PartBunchBase<double, 3> *bunch, const int turnnumber, const double t, const double tstep) override;
    // Record a single probe hit
    virtual void doOneCheck(PartBunchBase<double, 3> *bunch, const double t, unsigned int i);
    /// Hook for goOffline
    virtual void doGoOffline() override;

    bool fieldCorrection_m = true;
    double step_m; ///< Step size of the probe (bin width in histogram file)
    std::unique_ptr<PeakFinder> peakfinder_m; ///< Pointer to Peakfinder instance
    // The interpolated points registered by the probe
    Vector_t probePoint_m;
    Vector_t probeMom_m;
    double probeT_m;

    #ifdef WITH_UNIT_TESTS
    // enable doOneCheckTest if unit tests will be built
    FRIEND_TEST(ProbeTest, doOneCheckTest);
    #endif
};

#endif // CLASSIC_Probe_HH

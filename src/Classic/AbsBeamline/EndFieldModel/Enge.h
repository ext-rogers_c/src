/*
 *  Copyright (c) 2017, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ENDFIELDMODEL_ENGE_H_
#define ENDFIELDMODEL_ENGE_H_

#include <iostream>
#include <vector>

#include "AbsBeamline/EndFieldModel/EndFieldModel.h"

namespace endfieldmodel {

class Enge : public EndFieldModel {
  public:
    /** Default constructor */
    Enge() : a_m(), lambda_m(1.) {setEngeDiffIndices(10);}
    /** Builds Enge function with parameters a_0, a_1, ..., lambda and x0.
     *
     *  max_index is the maximum derivative that will be used in calculation
     *  if, after setup, you find you need to calculate higher derivatives, you
     *  can just call SetEngeDiffIndices(n) where n is the highest derivative
     *  you think you will need.
     */
    Enge(const std::vector<double> a, double x0, double lambda, int maxIndex)
        : a_m(a), lambda_m(lambda), x0_m(x0) {setEngeDiffIndices(maxIndex);}
    /** Destructor - no mallocs, so does nothing */
    ~Enge() {}

    /** Inheritable copy constructor - no mallocs, so does nothing */
    inline Enge* clone() const;

    /** Returns the enge parameters (a_i) */
    std::vector<double> getEngeParameters() const {return a_m;}

    /** Print a summary of the end field */
    inline std::ostream& print(std::ostream& out) const;
    
    /** Sets the enge parameters (a_i) */
    void                setEngeParameters(std::vector<double> a) {a_m = a;}

    /** Returns the value of lambda */
    inline double       getLambda() const {return lambda_m;}

    /** Sets the value of lambda */
    inline void         setLambda(double lambda) {lambda_m = lambda;}

    /** Returns the value of x0 */
    inline double       getX0() const {return x0_m;}

    /** Sets the value of x0 */
    inline void         setX0(double x0) {x0_m = x0;}

    /** Returns the value of the Enge function or its \f$n^{th}\f$ derivative.
     *
     *  Please call SetEngeDiffIndices(n) before calling if n > max_index
     */
    double getEnge(double x, int n) const;

    /** Returns \f$Enge(x-x0) + Enge(-x-x0)-1\f$ and its derivatives */
    inline double function(double x, int n) const;

    /** Returns \f$h(x)\f$ or its \f$n^{th}\f$ derivative.
     *
     *  Here \f$h(x) = a_0 + a_1 x/\lambda + a_2 x^2/lambda^2 + \ldots \f$
     *  Please call SetEngeDiffIndices(n) before calling if n > max_index
     */
    double hn(double x, int n) const;

    /** Returns \f$g(x)\f$ or its \f$n^{th}\f$ derivative.
     *
     *  Here \f$g(x) = 1+exp(h(x))\f$.
     *  Please call SetEngeDiffIndices(n) before calling if n > max_index
     */
    double gn(double x, int n) const;

    /** Recursively calculate the indices for Enge and H
     *
     *  This will calculate the indices for Enge and H that are required to
     *  calculate the differential up to order n.
     */
    static void   setEngeDiffIndices(size_t n);

    /** Return the indices for calculating the nth derivative of Enge ito g(x) */
    inline static std::vector< std::vector<int> > getQIndex(int n);

    /** Return the indices for calculating the nth derivative of g(x) ito h(x) */
    inline static std::vector< std::vector<int> > getHIndex(int n);
  private:
    std::vector<double> a_m;
    double              lambda_m, x0_m;

    /** Indexes the derivatives of enge in terms of g */
    static std::vector< std::vector< std::vector<int> > > q_m;
    /** Indexes the derivatives of g in terms of h */
    static std::vector< std::vector< std::vector<int> > > h_m;
};

std::vector< std::vector<int> > Enge::getQIndex(int n) {
  setEngeDiffIndices(n);
  return q_m[n];
}

std::vector< std::vector<int> > Enge::getHIndex(int n) {
  setEngeDiffIndices(n);
  return h_m[n];
}

double Enge::function(double x, int n) const {
  if (n == 0) {
    return (getEnge(x-x0_m, n)+getEnge(-x-x0_m, n))-1.;
  } else {
    if (n%2 != 0) return getEnge(x-x0_m, n)-getEnge(-x-x0_m, n);
    else          return getEnge(x-x0_m, n)+getEnge(-x-x0_m, n);
  }
}

std::ostream& Enge::print(std::ostream& out) const {
    out << "Enge with x0_m " << x0_m << " lambda " << lambda_m << " ";
    for (size_t i = 0; i < a_m.size(); ++i) {
        out << "a[" << i << "] " << a_m[i];        
    }
    return out;
}

Enge* Enge::clone() const {
    return new Enge(a_m, x0_m, lambda_m, 0);
}
}

#endif

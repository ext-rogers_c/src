/*
 *  Copyright (c) 2021, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ENDFIELDMODEL_ARCTAN_H_
#define ENDFIELDMODEL_ARCTAN_H_

#include <iostream>
#include "AbsBeamline/EndFieldModel/EndFieldModel.h"

namespace endfieldmodel {

/** Arctan end field that follows arctangent (inverse tangent) function
 *  
 *  f(x) = 1/pi [arctan((x+M/2)/L) - arctan((x-M/2)/L) ]
 *  
 *  For convenience each arctan is calculated separately. It may not be optimal.
 *  Note also that analytical solutions are provided to 13th order; this can
 *  involve calculating some higher powers of x, and I have not fiddled with 
 *  the maths much to improve stability. Caveat emptor!
 */
class Arctan : public EndFieldModel {
public:
    /** Default constructor; sets M, L to 1, 1 */
    Arctan();
    /** Constructor setting M and L */
    Arctan(double M, double L);
    /** Destructor */
    ~Arctan() {;}
    /** Print a string representation of the end field to the ostream */
    std::ostream& print(std::ostream& out) const;
    /** Calculate the n^th derivative of the function at position x 
     * 
     *  x: value at which the derivative is calculated
     *  n: order of derivative. 0 is the value, 1 is first derivative, etc. It
     *     is an error if n > 13. 
     * 
     */
    double function(double x, int n) const;
    /** Construct a copy of the Arctan (this is an inheritable copy constructor)
     */
    EndFieldModel* clone() const;
    /** Calculate the nth derivative of arctan(x) */
    double atanFunction(double x, int n) const;

private:
    Arctan(const Arctan& arctan);
    Arctan& operator=(const Arctan&);

    double m_m;
    double l_m;
};

}

#endif


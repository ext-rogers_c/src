/*
 *  Copyright (c) 2019, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CLASSIC_ABSBEAMLINE_RadialVoltageRFCavity_HH
#define CLASSIC_ABSBEAMLINE_RadialVoltageRFCavity_HH

#include <string>
#include <memory>
#include <vector>

#include <gsl/gsl_spline.h>

#include "Fields/EMField.h"
#include "BeamlineGeometry/StraightGeometry.h"
#include "AbsBeamline/Component.h"


/** Class RadialProfileRFCavity
  */

class RadialProfileRFCavity : public Component {
  public:
    typedef std::vector<std::pair<double, double> > profile_vector;
    /// Constructor with given name.
    explicit RadialProfileRFCavity(const std::string &name);
    RadialProfileRFCavity(const RadialProfileRFCavity &);
    RadialProfileRFCavity();
    RadialProfileRFCavity& operator=(const RadialProfileRFCavity &);

    ~RadialProfileRFCavity();

    /// Apply visitor to the RF Cavity.
    virtual void accept(BeamlineVisitor &) const;

    virtual ElementBase* clone() const;

    bool apply(const size_t &i,
               const double &t,
               Vector_t &E,
               Vector_t &B);
    bool apply(const Vector_t &R,
               const Vector_t &P,
               const double &t,
               Vector_t &E,
               Vector_t &B);
    bool applyToReferenceParticle(const Vector_t &R,
                                  const Vector_t &P,
                                  const double &t,
                                  Vector_t &E,
                                  Vector_t &B);
    virtual void initialise(PartBunchBase<double, 3> *bunch,
                            double &startField,
                            double &endField);
    void initialise();
    void finalise();
    bool bends() const {return false;}
    void getDimensions(double &zBegin, double &zEnd) const {}

    double getHeight() const {return halfHeight_m*2;}
    void setHeight(double fullHeight) {halfHeight_m = fullHeight/2;}

    double getLength() const {return _length;}
    void setLength(double length);
    
    double getFrequency() const {return frequency_m;}
    void setFrequency(double frequency) {frequency_m = frequency;}

    double getPhase() const {return phase_m;}
    void setPhase(double phase) {phase_m = phase;}

    profile_vector getAmplitudeProfile() const;
    void setAmplitudeProfile(profile_vector amplitudeProfile);

    bool getIsVertical() const;
    void setIsVertical(bool isVertical);

    StraightGeometry& getGeometry() {return geometry;}
    const StraightGeometry& getGeometry() const {return geometry;}

    /// Not implemented
    EMField &getField();
    /// Not implemented
    const EMField &getField() const;
  protected:

    double halfHeight_m;
    double _length;
    /// The cavity's geometry.
    StraightGeometry geometry;

  private:
    void initNull();
    // free the gsl spline and accel if it is not NULL
    void free();
    // alloc the gsl spline and accel; this is a memory leak if spline/alloc 
    // not free
    void alloc();
    std::vector<double> amplitudeProfile_m;
    std::vector<double> radialProfile_m;
    gsl_interp_accel *profileAcc_m = NULL;
    gsl_spline *profileSpline_m = NULL;
    double frequency_m = 0.;
    double phase_m  = 0.;
    bool isVertical_m = false;
};

#endif // CLASSIC_VirtualRFCavity_HH
#include "Classic/AbsBeamline/BoundingBox/CubicHexahedron.h"

namespace boundingbox {

CubicHexahedron::CubicHexahedron(
    BoundingBox* bb,
    Vector_t startPosition, Vector_t startNormal,
    Vector_t endPosition, Vector_t endNormal) : bb_m(bb),
      startPosition_m(startPosition*mm), startNormal_m(startNormal), 
      endPosition_m(endPosition*mm), endNormal_m(endNormal) {
}

std::ostream& CubicHexahedron::print(std::ostream& out) const {
    out << "CubicHexahedron bounding box" << std::endl;
    return out;
}

CubicHexahedron* CubicHexahedron::clone() const {
    CubicHexahedron* ch = new CubicHexahedron();
    ch->bb_m.reset(bb_m->clone());

    ch->startPosition_m = startPosition_m;
    ch->startNormal_m = startNormal_m;
    ch->endPosition_m = endPosition_m;
    ch->endNormal_m = endNormal_m;

    return ch;
}

} // namespace boundingbox

#include "Classic/AbsBeamline/BoundingBox/Cuboid.h"

namespace boundingbox {

Cuboid::Cuboid(double xMin, double xMax, 
               double yMin, double yMax,
               double zMin, double zMax) : xMin_m(xMin*mm), xMax_m(xMax*mm),
                                           yMin_m(yMin*mm), yMax_m(yMax*mm),
                                           zMin_m(zMin*mm), zMax_m(zMax*mm) {
}

std::ostream& Cuboid::print(std::ostream& out) const {
    out << "Cuboid bounding box" << std::endl;
    return out;
}

Cuboid* Cuboid::clone() const {
    Cuboid* cb = new Cuboid();
    cb->xMin_m = xMin_m;
    cb->xMax_m = xMax_m;
    cb->yMin_m = yMin_m;
    cb->yMax_m = yMax_m;
    cb->zMin_m = zMin_m;
    cb->zMax_m = zMax_m;
    return cb;
}

} // namespace boundingbox

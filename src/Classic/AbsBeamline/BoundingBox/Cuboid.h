#ifndef BOUNDINGBOX_CUBOID_H_
#define BOUNDINGBOX_CUBOID_H_ 1

#include <iostream>
#include "Algorithms/Vektor.h"

#include "AbsBeamline/BoundingBox/BoundingBox.h"

namespace boundingbox {
  
/** Cuboid shaped bounding box
 */
class Cuboid : public BoundingBox {
public:
    /** Constructor */
    Cuboid(double xMin, double xMax,
          double yMin, double yMax,
          double zMin, double zMax);
    /** Default Constructor; initialises everything to 0 */
    Cuboid() {}

    /** Virtual destructor */
    virtual ~Cuboid() {;}
    /** Print the bb details */
    virtual std::ostream& print(std::ostream& out) const;

    /** Return true if position is outside the bounding box
    */
    inline virtual bool isOutside(Vector_t position) const;

    /** Inheritable copy constructor */
    virtual Cuboid* clone() const;
private:
    double xMin_m = 0;
    double xMax_m = 0;
    double yMin_m = 0;
    double yMax_m = 0;
    double zMin_m = 0;
    double zMax_m = 0;

    Cuboid(const Cuboid&) {}
    Cuboid& operator=(const Cuboid&) {return *this;}
};

bool Cuboid::isOutside(Vector_t position) const {
   return position[0] < xMin_m || position[0] > xMax_m ||
          position[1] < yMin_m || position[1] > yMax_m ||
          position[2] < zMin_m || position[2] > zMax_m;
}

} // namespace boundingbox

#endif // BOUNDINGBOX_BOUNDINGBOX_H_
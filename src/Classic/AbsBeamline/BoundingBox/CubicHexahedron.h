#ifndef BOUNDINGBOX_CUBICHEX_H_
#define BOUNDINGBOX_CUBICHEX_H_

#include <iostream>
#include "Algorithms/Vektor.h"
#include "Classic/AbsBeamline/BoundingBox/BoundingBox.h"


namespace boundingbox {
  
/** Bounding box is a generic boundingbox with extra slice through start and end faces
 *  
 *  Note that the start and end face does not affect the placements (i.e. this
 *  has nothing to do with the actual start and end vector for the previous/next
 *  element)
 */
class CubicHexahedron : public BoundingBox {
public:
    /** Constructor */
    CubicHexahedron(BoundingBox* box,
                    Vector_t startPosition, Vector_t startNormal,
                    Vector_t endPosition, Vector_t endNormal);

    /** Default Constructor; initialises everything to 0 */
    CubicHexahedron() {}

    /** Virtual destructor */
    virtual ~CubicHexahedron() {;}
    /** Print the bb details */
    virtual std::ostream& print(std::ostream& out) const;

    /** Return true if position is outside the bounding box
    */
    inline virtual bool isOutside(Vector_t position) const;

    /** Inheritable copy constructor */
    virtual CubicHexahedron* clone() const;
private:
    std::unique_ptr<BoundingBox> bb_m;

    Vector_t startPosition_m;
    Vector_t startNormal_m;
    Vector_t endPosition_m;
    Vector_t endNormal_m;

    CubicHexahedron(const CubicHexahedron&) {}
    CubicHexahedron& operator=(const CubicHexahedron&) {return *this;}
};

bool CubicHexahedron::isOutside(Vector_t position) const {
    if (bb_m->isOutside(position)) {
        return true;
    }
    Vector_t posTransformed = position-startPosition_m;
    double normProd = posTransformed(0)*startNormal_m(0)+
                      posTransformed(1)*startNormal_m(1)+
                      posTransformed(2)*startNormal_m(2);
    if (normProd < 0.) {
        return true;
    }
    posTransformed = position-endPosition_m;
    normProd = posTransformed(0)*endNormal_m(0)+
               posTransformed(1)*endNormal_m(1)+
               posTransformed(2)*endNormal_m(2);
    if (normProd > 0.) {
        return true;
    }
    return false;
}


} // namespace boundingbox

#endif // BOUNDINGBOX_CUBICHEX_H_
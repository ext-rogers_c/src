#ifndef BOUNDINGBOX_BOUNDINGBOX_H_
#define BOUNDINGBOX_BOUNDINGBOX_H_

#include <iostream>
#include "Algorithms/Vektor.h"


namespace boundingbox {
  
/** Abstraction layer for bounding boxes
 * 
 *  Enables support for odd shapes in bounding boxes
 */
class BoundingBox {
public:
    /** Virtual destructor */
    virtual ~BoundingBox() {;}
    /** Print the bb details */
    virtual std::ostream& print(std::ostream& out) const = 0;
    /** Return true if position is outside the bounding box
     */
    virtual bool isOutside(Vector_t position) const = 0;

    /** Inheritable copy constructor */
    virtual BoundingBox* clone() const = 0;
    double mm = 1e3;
};

} // namespace boundingbox

#endif // BOUNDINGBOX_BOUNDINGBOX_H_

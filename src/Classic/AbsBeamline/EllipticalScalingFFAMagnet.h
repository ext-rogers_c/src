
#include "Fields/BMultipoleField.h"
#include "BeamlineGeometry/PlanarArcGeometry.h"
#include "AbsBeamline/EndFieldModel/EndFieldModel.h"
#include "AbsBeamline/Component.h"

#ifndef ABSBEAMLINE_EllipticalScalingFFAMagnet_H
#define ABSBEAMLINE_EllipticalScalingFFAMagnet_H

/** Sector bending magnet with an FFA-style field index and spiral end shape
 */

class EllipticalScalingFFAMagnet : public Component {
  public:
    /** Construct a new EllipticalScalingFFAMagnet
     *
     *  \param name User-defined name of the EllipticalScalingFFAMagnet
     */
    explicit EllipticalScalingFFAMagnet(const std::string &name);

    /** Destructor - deletes map */
    ~EllipticalScalingFFAMagnet();

    /** Inheritable copy constructor */
    ElementBase* clone() const override;

    /** Calculate the field at the position of the ith particle
     *
     *  \param i index of the particle event override; field is calculated at this
     *         position
     *  \param t time at which the field is to be calculated
     *  \param E calculated electric field - always 0 (no E-field)
     *  \param B calculated magnetic field
     *  \returns true if particle is outside the field map
     */
    bool apply(const size_t &i, const double &t, Vector_t &E, Vector_t &B) override;

    /** Calculate the field at some arbitrary position
     *
     *  \param R position in the local coordinate system of the bend
     *  \param P not used
     *  \param t not used
     *  \param E not used
     *  \param B calculated magnetic field
     *  \returns true if particle is outside the field map, else false
     */
    inline bool apply(const Vector_t &R, const Vector_t &P, const double &t,
               Vector_t &E, Vector_t &B) override;

    /** Calculate the field at some arbitrary position in cartesian coordinates
     *
     *  \param R position in the local coordinate system of the bend, in
     *           cartesian coordinates defined like (x, y, z)
     *  \param B calculated magnetic field defined like (Bx, By, Bz)
     *  \returns true if particle is outside the field map, else false
     */
    bool getFieldValue(const Vector_t &R, Vector_t &B) const;

    /** Calculate the field at some arbitrary position in elliptical coordinates
     *
     *  \param R position in the local coordinate system of the bend, in
     *           elliptical polar coordinates defined like (mu, y, nu), with
     *           0 <= nu <= 2pi and mu > 0
     *  \param B calculated magnetic field defined like (Bmu, By, Bnu)
     *  \returns true if particle is outside the field map, else false
     */
    bool getFieldValueElliptical(const Vector_t &R, Vector_t &B) const;

     /** Initialise the EllipticalScalingFFAMagnet
      *
      *  \param bunch the global bunch object
      *  \param startField not used
      *  \param endField not used
      */
      void initialise(PartBunchBase<double, 3> *bunch, double &startField, double &endField) override;

     /** Initialise the EllipticalScalingFFAMagnet
      *
      *  Sets up the field expansion and the geometry; call after changing any
      *  field parameters
      */
    void initialise();

     /** Finalise the EllipticalScalingFFAMagnet - sets bunch to NULL */
    void finalise() override;

    /** Return true - EllipticalScalingFFAMagnet always bends the reference particle */
    inline bool bends() const override;

    /** Not implemented */
    void getDimensions(double &zBegin, double &zEnd) const override {}

    /** Return the cell geometry */
    BGeometryBase& getGeometry() override;

    /** Return the cell geometry */
    const BGeometryBase& getGeometry() const override;

    /** Return a dummy (0.) field value (what is this for?) */
    EMField &getField() override;

    /** Return a dummy (0.) field value (what is this for?) */
    const EMField &getField() const override;

    /** Accept a beamline visitor */
    void accept(BeamlineVisitor& visitor) const override;

    /** Get tan delta - delta is the spiral angle */
    double getTanDelta() const {return tanDelta_m;}

    /** Set tan delta - delta is the spiral angle */
    void setTanDelta(double tanDelta) {tanDelta_m = tanDelta;}

    /** Get the field index k */
    double getFieldIndex() const {return k_m;}

    /** Set the field index k */
    void setFieldIndex(double k) {k_m = k;}

    /** Get the dipole constant B_0 */
    double getDipoleConstant() const {return Bz_m;}

    /** Set the dipole constant B_0 */
    void setDipoleConstant(double Bz) {Bz_m = Bz;}

    /** Get the radius constant R_0 */
    double getMu0() const {return mu0_m;}

    /** Set the radius constant R_0
     *
     *  Note that this also calls resetCentre
     */
    inline void setMu0(double mu0);

    /** Get the aspect ratio a */
    double getA() const {return a_m;}

    /** Set the aspect ratio
     *
     *  Note that this also calls resetCentre
     */
    inline void setA(double aspectRatio);

    /** Get the centre of the sector */
    Vector_t getCentre() const {return centre_m;}

    /** Set the centre of the sector
     * 
     *  Because calls to setMu0 and setA call resetCentre, if you want to
     *  setCentre to something different, this must be done after setA/setMu0.
     */
    void setCentre(Vector_t centre) {centre_m = centre;}

    /** Set the centre based on a_m and mu0_m so that a_m cosh(mu0_m) is at 
     *  cartesian (0, 0) in the local coordinate system.
     */
    inline void resetCentre();

    /** Get the fringe field
     *
     *  Returns the fringe field model; EllipticalScalingFFAMagnet retains ownership of the
     *  returned memory.
     */
    endfieldmodel::EndFieldModel* getEndField() const {return endField_m;}

    /** Set the fringe field
      * 
      * - endField: the new fringe field; EllipticalScalingFFAMagnet takes ownership of the
      *   memory associated with endField.
      */
    void setEndField(endfieldmodel::EndFieldModel* endField);

    /** Get the maximum power of y modelled in the off-midplane expansion; 
     */
    size_t getMaxOrder() const {return maxOrder_m;}

    /** Set the maximum power of y modelled in the off-midplane expansion;
     */
    void setMaxOrder(size_t maxOrder) {maxOrder_m = maxOrder;}

    /** Get the offset of the magnet centre from the start 
     */
    double getNuStart() const {return phiStart_m;}

    /** Set the offset of the magnet centre from the start 
     */
    void setNuStart(double nuStart) {phiStart_m = nuStart;}

    /** Get the offset of the magnet end from the start 
     */
    double getNuEnd() const {return phiEnd_m;}

    /** Set the offset of the magnet end from the start 
     */
    void setNuEnd(double nuEnd) {phiEnd_m = nuEnd;}

    /** Get the maximum radius
     */
    double getMuMin() const {return muMin_m;}

    /** Set the maximum radius 
     */
    void setMuMin(double muMin) {muMin_m = muMin;}

    /** Get the maximum radius
     */
    double getMuMax() const {return muMax_m;}

    /** Set the maximum radius 
     */
    void setMuMax(double muMax) {muMax_m = muMax;}

    /** Get the maximum azimuthal displacement from \psi=0
     */
    double getAzimuthalExtent() const {return azimuthalExtent_m;}

    /** Set the maximum azimuthal displacement from \psi=0
     */
    void setAzimuthalExtent(double azimuthalExtent) {azimuthalExtent_m = azimuthalExtent;}

    /** Get the maximum vertical displacement from the midplane
     */
    double getVerticalExtent() const {return verticalExtent_m;}

    /** Set the maximum vertical displacement from the midplane
     */
    void setVerticalExtent(double verticalExtent) {verticalExtent_m = verticalExtent;}

    /** Return the calculated df coefficients */
    std::vector<std::vector<double> > getDfCoefficients() {return dfCoefficients_m;}

    /** convert from cartesian to elliptical coordinates **/
    void convertToElliptical(const double& x, const double& y, double& mu, double& nu) const;

    /** convert from elliptical to cartesian coordinates **/
    void convertToCartesian(const double& mu, const double& nu, double& x, double& y) const;

private:
    /** Calculate the df coefficients, ready for field generation
     *
     *  Must be called following any update to the the field parameters, in
     *  order for correct field to be calculated.
     */
    void calculateDfCoefficients();

    /** Copy constructor */
    EllipticalScalingFFAMagnet(const EllipticalScalingFFAMagnet &right);

    EllipticalScalingFFAMagnet& operator=(const EllipticalScalingFFAMagnet& rhs);
    PlanarArcGeometry planarArcGeometry_m;
    BMultipoleField dummy;

    size_t maxOrder_m = 0;
    double tanDelta_m = 0.;
    double k_m = 0.;
    double Bz_m = 0.;
    double mu0_m = 0.;
    double a_m = 0.;
    double muMin_m = 0.; // minimum radius
    double muMax_m = 0.; // maximum radius
    double phiStart_m = 0.; // offsets this element
    double phiEnd_m = 0.; // used for placement of next element
    double azimuthalExtent_m = 0.; // maximum distance used for field calculation
    double verticalExtent_m = 0.; // maximum allowed distance from the midplane
    Vector_t centre_m;
    endfieldmodel::EndFieldModel* endField_m = NULL;
    const double fp_tolerance = 1e-18;
    std::vector<std::vector<double> > dfCoefficients_m;
};

void EllipticalScalingFFAMagnet::resetCentre() {
    centre_m = Vector_t(-a_m*cosh(mu0_m), 0., 0.);
}

void EllipticalScalingFFAMagnet::setMu0(double mu0) {
    mu0_m = mu0;
    resetCentre();
}

void EllipticalScalingFFAMagnet::setA(double aspectRatio) {
    a_m = aspectRatio;
    resetCentre();
}


bool EllipticalScalingFFAMagnet::apply(const Vector_t &R, const Vector_t &P,
                    const double &t, Vector_t &E, Vector_t &B) {
    return getFieldValue(R, B);
}

#endif


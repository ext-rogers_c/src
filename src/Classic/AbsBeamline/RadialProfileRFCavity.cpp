/*
 *  Copyright (c) 2019, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include <math.h>

#include <iostream>
#include <sstream>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include "Physics/Physics.h"
#include "Utilities/GeneralClassicException.h"
#include "Algorithms/PartBunchBase.h"
#include "AbsBeamline/BeamlineVisitor.h"

#include "AbsBeamline/RadialProfileRFCavity.h"

RadialProfileRFCavity::RadialProfileRFCavity(const std::string &name)
        : Component(name), profileAcc_m(NULL), profileSpline_m(NULL) {
}

RadialProfileRFCavity::RadialProfileRFCavity(const RadialProfileRFCavity & rhs)
                 : Component(rhs), profileAcc_m(NULL), profileSpline_m(NULL) {
    *this = rhs;
}

RadialProfileRFCavity::RadialProfileRFCavity()
                 : Component(), profileAcc_m(NULL), profileSpline_m(NULL) {
}

RadialProfileRFCavity& RadialProfileRFCavity::operator=(const RadialProfileRFCavity & rhs) {
    halfHeight_m = rhs.halfHeight_m;
    _length = rhs._length;
    geometry = rhs.geometry;

    amplitudeProfile_m = rhs.amplitudeProfile_m;
    radialProfile_m = rhs.radialProfile_m;
    frequency_m = rhs.frequency_m;
    phase_m = rhs.phase_m;

    initialise();
    return *this;
}

RadialProfileRFCavity::~RadialProfileRFCavity() {
    free();
}


bool sort_predicate(std::pair<double, double> a, std::pair<double, double> b) {
    return a.first < b.first;
}
  
void RadialProfileRFCavity::setAmplitudeProfile(profile_vector amplitudeProfile) {
    std::sort(amplitudeProfile.begin(), amplitudeProfile.end(), sort_predicate);
    size_t len = amplitudeProfile.size();
    
    radialProfile_m = std::vector<double>(len, 0.);
    amplitudeProfile_m = std::vector<double>(len, 0.);   

    if (len < 4) {
        throw GeneralClassicException(
            "RadialProfileRFCavity::setAmplitudeProfile",
            "Need at least 4 points in the profile");
    }
    for (size_t i = 0; i < len; ++i) {
        radialProfile_m[i] = amplitudeProfile[i].first;
        amplitudeProfile_m[i] = amplitudeProfile[i].second;
        if (i > 0 and radialProfile_m[i] <= radialProfile_m[i-1]) {
            std::stringstream sstr;
            sstr << "Element " << i-1 << " had r: " << radialProfile_m[i-1]
                 << " and element " << i << " had r: " << radialProfile_m[i]
                 << "\n";
            throw GeneralClassicException(
                "RadialProfileRFCavity::setAmplitudeProfile",
                "Radii must be strictly increasing. "+sstr.str());
        }
    }
    initialise();
}

RadialProfileRFCavity::profile_vector
                            RadialProfileRFCavity::getAmplitudeProfile() const {
    size_t len = radialProfile_m.size();
    profile_vector prof(len);
    for (size_t i = 0; i < len; ++i) {
        prof[i] = std::pair<double, double>(radialProfile_m[i],
                                            amplitudeProfile_m[i]);
    }
    return prof;
}


void RadialProfileRFCavity::accept(BeamlineVisitor& visitor) const {
    visitor.visitRadialProfileRFCavity(*this);
}


void RadialProfileRFCavity::alloc() {
    int len = amplitudeProfile_m.size();
    if (len < 4) {
       return;
    }
    profileAcc_m = gsl_interp_accel_alloc();
    profileSpline_m = gsl_spline_alloc(gsl_interp_cspline, len);
    gsl_spline_init(profileSpline_m, &radialProfile_m[0], &amplitudeProfile_m[0], len);
    
}

void RadialProfileRFCavity::free() {
    if (profileAcc_m != NULL) {
        gsl_interp_accel_free(profileAcc_m);
        profileAcc_m = NULL;
    }
    if (profileSpline_m != NULL) {
        gsl_spline_free(profileSpline_m);
        profileSpline_m = NULL;
    }
}

bool RadialProfileRFCavity::apply(const Vector_t &R,
                                  const Vector_t &P,
                                  const double &t,
                                  Vector_t &E,
                                  Vector_t &B) {
    // flip x and y to make a vertically stacked cavity
    Vector_t rTmp(R);
    if (isVertical_m) {
        rTmp[1] = R[0];
        rTmp[0] = R[1];
    }
    // bounding box
    if (rTmp[0] < radialProfile_m[0] ||
        rTmp[0] > radialProfile_m.back() ||
        abs(rTmp[1]) > halfHeight_m ||
        rTmp[2] < 0. ||
        rTmp[2] > _length) {
        return true;
    }
    double E0 = gsl_spline_eval(profileSpline_m, rTmp[0], profileAcc_m);
    E[2] = E0*sin(Physics::two_pi * (frequency_m * t + phase_m));
    return false;
}

bool RadialProfileRFCavity::apply(const size_t &i,
                             const double &t,
                             Vector_t &E,
                             Vector_t &B) {
    return apply(RefPartBunch_m->R[i], RefPartBunch_m->P[i], t, E, B);
}

bool RadialProfileRFCavity::applyToReferenceParticle(const Vector_t &R,
                                                     const Vector_t &P,
                                                     const double &t,
                                                     Vector_t &E,
                                                     Vector_t &B) {
    return apply(R, P, t, E, B);
}

ElementBase* RadialProfileRFCavity::clone() const {
    RadialProfileRFCavity* copy = new RadialProfileRFCavity();
    *copy = *this;
    return copy;
}

void RadialProfileRFCavity::initialise(PartBunchBase<double, 3> *bunch,
                         double &startField,
                         double &endField) {
    initialise();
}

 void RadialProfileRFCavity::initialise() {
    free();
    alloc();
}

void RadialProfileRFCavity::finalise() {
}

void RadialProfileRFCavity::setLength(double length) {
    _length = length;
    geometry.setElementLength(_length);
}


EMField &RadialProfileRFCavity::getField() {
  throw GeneralClassicException("VariableRFCavity",
                      "No field defined for VariableRFCavity");
}

const EMField &RadialProfileRFCavity::getField() const {
  throw GeneralClassicException("RadialProfileRFCavity",
                      "No field defined for VariableRFCavity");
}

bool RadialProfileRFCavity::getIsVertical() const {
  return isVertical_m;
}


void RadialProfileRFCavity::setIsVertical(bool isVertical) {
  isVertical_m = isVertical;
}

#include "Fields/Interpolation/MMatrix.h"
#include "Fields/Interpolation/LeastSquaresSolveFactory.h"
#include "Utilities/GeneralClassicException.h"

namespace interpolation {
LeastSquaresSolveFactory::LeastSquaresSolveFactory(
          size_t polynomialOrder,
          const std::vector< std::vector<double> >& points
    ) {
    // Algorithm: we want g(x) = old_f(x) + new_f(x), where old_f has polynomial
    // terms in poly, new_f has all the rest. Use value - old_f(point) as input and
    // do LLS forcing old_f(x) polynomial terms to 0
    // Nb: in this constrained case we have to go through the output vector and
    // treat each like a 1D vector
    points_m = points;
    nPoints_m    = points.size();
    polynomialOrder_m = polynomialOrder;
    if (nPoints_m < 1) {
        throw GeneralClassicException(
            "LeastSquaresSolveFactory(...)",
            "points size should be at least 1."
        );
    }
    for (auto& x: points) {
        if (x.size() != points_m[0].size()) {
            throw GeneralClassicException(
              "LeastSquaresSolveFactory(...)",
              "Each point should be of the same length"
            );
        }
        if (x.size() == 0) {
            throw GeneralClassicException(
              "LeastSquaresSolveFactory(...)",
              "Each point should be at least length 1"
            );
        }
    }
    weights_m = std::vector<double>(nPoints_m, 1);


    pointDim_m   = points_m[0].size();
    nCoeffsNew_m = SquarePolynomialVector::NumberOfPolynomialCoefficients(
                                            pointDim_m,
                                            polynomialOrder_m);
    indexByVector_m = std::vector< std::vector<int> >(nCoeffsNew_m);
    for (size_t i = 0; i < indexByVector_m.size(); ++i) {
        indexByVector_m[i] = SquarePolynomialVector::IndexByVector(i, pointDim_m);
    }

    // newPolyMap1D is for calculating the x^n
    SquarePolynomialVector newPolyMap1D(pointDim_m, MMatrix<double>(1, nCoeffsNew_m));
    tempFx_m = std::vector< std::vector<double> >(nPoints_m,
                                                  std::vector<double>(nCoeffsNew_m, -999.) );
    for (size_t i = 0; i < nPoints_m; ++i) {
        newPolyMap1D.MakePolyVector(&points_m[i][0], &tempFx_m[i][0]);
    }
}

SquarePolynomialVector LeastSquaresSolveFactory::solve
                            (const std::vector< std::vector<double> >& values) {
    if (values.size() != nPoints_m) {
        throw GeneralClassicException(
            "LeastSquaresSolveFactory(...)",
            "values size should be the same length as points."
        );
    }
    for (auto& y: values) {
        if (y.size() != values[0].size()) {
            throw GeneralClassicException(
              "LeastSquaresSolveFactory(...)",
              "Each value should be of the same length"
            );
        }
        if (y.size() == 0) {
            throw GeneralClassicException(
              "LeastSquaresSolveFactory(...)",
              "Each value should be at least length 1"
            );
        }
    }
    // check coefficients are compatible with values - can only do this once we
    // know the dimension of the codomain (length of each value)
    for (auto& c: coeffs_m) {
        if (c.OutVariable() >= int(values[0].size()) || c.OutVariable() < 0) {
            throw GeneralClassicException(
              "LeastSquaresSolveFactory(...)",
              "Polynomial coefficient with codomain incompatible with values dimension"
            );
        }
    }
    values_m = values;
    valueDim_m   = values_m[0].size();
    subtractOldValues();
    A_m = MMatrix<double>(valueDim_m, nCoeffsNew_m, 0.);
    for (size_t dim = 0; dim < valueDim_m ; dim++) {
        solveOneDimension(dim);
    }

    // now merge the oldCoeffs back into A
    for (size_t i = 0; i < coeffs_m.size(); ++i) {
        for (size_t j = 0; j < indexByVector_m.size(); ++j) {
            if (indexByVector_m[j] == coeffs_m[i].InVariables()) {
                A_m(coeffs_m[i].OutVariable()+1, j+1) = coeffs_m[i].Coefficient();
                break;
            }
        }
    }
    return SquarePolynomialVector(pointDim_m, A_m);
}

void LeastSquaresSolveFactory::subtractOldValues() {
    MMatrix<double> empty(valueDim_m, nCoeffsNew_m, 0.);
    SquarePolynomialVector oldMap(pointDim_m, empty);
    oldMap.ResetCoefficients(coeffs_m);
    for (size_t i = 0; i < points_m.size(); ++i) {
        std::vector<double> point = points_m[i];
        std::vector<double> oldValue(valueDim_m);
        oldMap.F(&point[0], &oldValue[0]);
        for (size_t j = 0; j < oldValue.size(); ++j) {
            values_m[i][j] -= oldValue[j];
        }
    }
}

void LeastSquaresSolveFactory::solveOneDimension(size_t dim) {
    // oldPolyMap1D is for calculating old_f(x) for y_dim
    // first find the coefficients which pertain to this axis in codomain
    // make an index of variables that need calculation
    std::vector<int> needsCalculation;
    for (size_t i = 0; i < nCoeffsNew_m; ++i) {
        bool exists = false;
        for (size_t j = 0; j < coeffs_m.size(); ++j) {
            if (coeffs_m[j].OutVariable() == int(dim) &&
                coeffs_m[j].InVariables() == indexByVector_m[i]) {
                exists = true;
            }
        }
        if (!exists) {
            needsCalculation.push_back(i);
        }
    }
    size_t deltaCoeff = needsCalculation.size();
    if (deltaCoeff == 0) {
        return;
    }

    // now solve for those elements that are not fixed
    MVector<double> Fy(deltaCoeff, 0);
    MMatrix<double> F2(deltaCoeff, deltaCoeff,  0.);

    // optimisation note - this algorithm spends all its time in this loop
    for (size_t i = 0; i < nPoints_m && needsCalculation.size() > 0; ++i) {
        for (size_t k = 0; k < deltaCoeff; k++) {
            Fy(k+1) += values_m[i][dim]*tempFx_m[i][needsCalculation[k]]*weights_m[i];
            for (size_t j = 0; j < deltaCoeff; ++j) {
                F2(j+1, k+1) += tempFx_m[i][needsCalculation[k]]*
                                tempFx_m[i][needsCalculation[j]]*weights_m[i];
            }
        }
    }

    MMatrix<double> F2_inverse;
    try {
        F2_inverse = F2.inverse();
    } catch (...) {
        throw GeneralClassicException(
            "LeastSquaresSolveFactory(...)",
            "Failed to invert the matrix during least squares fit"
        );
    }
    MVector<double> AVec = F2_inverse * Fy;
    for (size_t i = 0; i < deltaCoeff; ++i) {
        A_m(dim+1, needsCalculation[i]+1) = AVec(i+1);
    }
}

void LeastSquaresSolveFactory::setWeights(std::vector<double> weights) {
    if (weights.size() == 0) {
        weights = std::vector<double>(nPoints_m, 1.);
    }
    if (weights.size() != nPoints_m) {
        throw GeneralClassicException(
          "LeastSquaresSolveFactory(...)",
          "Weights should be the same length as points"
        );
    }
    weights_m = weights;
}

void LeastSquaresSolveFactory::setCoefficients
                      (const std::vector< PolynomialCoefficient >& coeffs) {
    for (auto& c: coeffs) {
        std::vector<int> indexByPower = c.InVariablesByPower(pointDim_m);
        for (auto& i: indexByPower) {
            if (i > int(polynomialOrder_m)) {
                throw GeneralClassicException(
                  "LeastSquaresSolveFactory(...)",
                  "Polynomial coefficient with power higher than polynomialOrder"
                );
            }
        }
        std::vector<int> indexByVector = c.InVariables();
        for (auto& i: indexByVector) {
            if (i >= int(pointDim_m) or i < 0) {
                throw GeneralClassicException(
                  "LeastSquaresSolveFactory(...)",
                  "Polynomial coefficient with domain incompatible with points dimension"
                );
            }
        }
    }
    coeffs_m = coeffs;
}


}
#include <vector>

#include "Fields/Interpolation/SquarePolynomialVector.h"
#include "Fields/Interpolation/PolynomialCoefficient.h"

#ifndef LeastSquaresSolveFactory_hh
#define LeastSquaresSolveFactory_hh

namespace interpolation {

/** \class LeastSquaresSolveFactory
 *  \brief LeastSquaresSolveFactory is a factory class for solving a set of
 *  linear equations to generate a polynomial based on nearby points. In this
 *  case the solve is done by a least squares fit to a number of points in a
 *  multidimensional space.
 *
 *  Finds the polynomial P that minimises:
 *          sum_i ( w_i (\vec{y_i} - P(\vec{x_i}))^2.
 *          y_i = values[i],
 *          x_i = points[i],
 *          w_i = weightFunction(x_i) OR weights[i],
 *          polynomialOrder is the order of P
 */
class LeastSquaresSolveFactory {
  public:
    /** Set up the problem.
     *  - polynomialOrder: order of the target polynomial. Note because we use
     *            SquarePolynomialVector 1 means all combinations of domain
     *            having power 1 (i.e. Product_i x_i^{n_i} for all values of
     *            n_i <= polynomialOrder)
     *  - points: vector of abscissa each being a vector of size equal to the
     *            dimension of the abscissa space (domain)
     */
    LeastSquaresSolveFactory(
          size_t polynomialOrder,
          const std::vector< std::vector<double> >&  points
    );

    /** Set the weighting for the solver.
     *  - weights: weight the solver by 'weights'. Should have length equal to
     *            the length of points or 0, in which case the solver will
     *            fit higher weighted points more closely. If the length is 0,
     *            all points will be weighted equally.
     *
     */
    void setWeights(std::vector<double> weights);

    /** Set predefined coefficients which constrain the fit.
     *  - coeffs: These coefficients will be fixed and not fitted by the solver.
     *            Coeffs should have same dimension as points and should have
     *            "order" <= polynomialOrder
     *
     */
    void setCoefficients(const std::vector< PolynomialCoefficient >& coeffs);
    
    /** Solve for a given set of values.
     *  - values: vector of ordinates each being a vector of size equal to the
     *            dimension of the ordinate space (codomain)
     */
    SquarePolynomialVector solve
                            (const std::vector< std::vector<double> >& values);
  private:

    void solveOneDimension(size_t dim);
    void subtractOldValues();
    
    std::vector<std::vector<double>> oldValues_m;

    MMatrix<double> A_m;
    std::vector<double> weights_m;
    std::vector< std::vector<int> > indexByVector_m;
    std::vector< std::vector<double> > tempFx_m;
    std::vector< PolynomialCoefficient > coeffs_m;
    std::vector< std::vector<double> >  points_m;
    std::vector< std::vector<double> > values_m;

    size_t nPoints_m;
    size_t pointDim_m;
    size_t valueDim_m;
    size_t nCoeffsNew_m;
    size_t polynomialOrder_m;
};

}

#endif

/*
 *  Copyright (c) 2012, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include "Elements/OpalOffset/OpalLocalCartesianOffset.h"

#include <string>

#include "AbsBeamline/Offset.h"
#include "Utilities/OpalException.h"
#include "Elements/OpalElement.h"

#include "Attributes/Attributes.h"

namespace OpalOffset {

const std::string OpalLocalCartesianOffset::doc_string =
    "The \"LOCAL_CARTESIAN_OFFSET\" element defines an offset in cartesian "
    "coordinates.  The offset is defined in the coordinate system of the end "
    "face of the previous element in the line.";

OpalLocalCartesianOffset::OpalLocalCartesianOffset()
       : OpalElement(int(SIZE),
                     "LOCAL_CARTESIAN_OFFSET",
                     doc_string.c_str()) {
    itsAttr[END_POSITION_X] = Attributes::makeReal("END_POSITION_X",
             "x component of the position of the end of the offset [m].");
    itsAttr[END_POSITION_Y] = Attributes::makeReal("END_POSITION_Y",
             "y component of the position of the end of the offset [m].");
    itsAttr[END_POSITION_Z] = Attributes::makeReal("END_POSITION_Z",
             "z component of the position of the end of the offset [m].");
    itsAttr[END_NORMAL_X] = Attributes::makeReal("END_NORMAL_X",
             "x component of the normal of the end face of the offset.");
    itsAttr[END_NORMAL_Y] = Attributes::makeReal("END_NORMAL_Y",
             "y component of the normal of the end face of the offset.");
    itsAttr[END_ROTATION_X] = Attributes::makeReal("END_ROTATION_X",
       "Angle of rotation about the x axis [rad]. It is an error to define "
       "END_NORMAL and END_ROTATION. Rotations are applied in the order "
       "rotate about X then about Y then about Z.");
    itsAttr[END_ROTATION_Y] = Attributes::makeReal("END_ROTATION_Y",
       "Angle of rotation about the y axis [rad]. It is an error to define "
       "END_NORMAL and END_ROTATION. Rotations are applied in the order "
       "rotate about X then about Y then about Z.");
    itsAttr[END_ROTATION_Z] = Attributes::makeReal("END_ROTATION_Z",
       "Angle of rotation about the z axis [rad]. It is an error to define "
       "END_NORMAL and END_ROTATION. Rotations are applied in the order "
       "rotate about X then about Y then about Z.");
    registerRealAttribute("END_POSITION_X");
    registerRealAttribute("END_POSITION_Y");
    registerRealAttribute("END_POSITION_Z");
    registerRealAttribute("END_NORMAL_X");
    registerRealAttribute("END_NORMAL_Y");
    registerRealAttribute("END_ROTATION_X");
    registerRealAttribute("END_ROTATION_Y");
    registerRealAttribute("END_ROTATION_Z");

    registerOwnership();

    setElement((new Offset("LOCAL_CARTESIAN_OFFSET"))->makeAlignWrapper());
}

OpalLocalCartesianOffset* OpalLocalCartesianOffset::clone() {
    return new OpalLocalCartesianOffset(this->getOpalName(), this);
}


OpalLocalCartesianOffset* OpalLocalCartesianOffset::clone(const std::string &name) {
    return new OpalLocalCartesianOffset(name, this);
}

void OpalLocalCartesianOffset::print(std::ostream& out) const {
    OpalElement::print(out);
}

OpalLocalCartesianOffset::OpalLocalCartesianOffset(const std::string &name, OpalLocalCartesianOffset *parent):
    OpalElement(name, parent) {
    setElement(new Offset(name));
}

OpalLocalCartesianOffset::~OpalLocalCartesianOffset() {}

void OpalLocalCartesianOffset::fillRegisteredAttributes
                                     (const ElementBase &base, ValueFlag flag) {
    OpalElement::fillRegisteredAttributes(base, flag);
    const Offset* offset = dynamic_cast<const Offset*>(&base);
    if (offset == NULL) {
        throw OpalException("OpalLocalCartesianOffset::fillRegisteredAttributes",
                            "Failed to cast ElementBase to an offset");
    }

    Euclid3D trans = offset->getGeometry().getTotalTransform();
    attributeRegistry["END_POSITION_X"]->setReal(trans.getVector()(2));
    attributeRegistry["END_POSITION_Y"]->setReal(-trans.getVector()(0));
    attributeRegistry["END_POSITION_Z"]->setReal(trans.getVector()(1));
    attributeRegistry["END_ROTATION_X"]->setReal(rotationX_m);
    attributeRegistry["END_ROTATION_Y"]->setReal(rotationY_m);
    attributeRegistry["END_ROTATION_Z"]->setReal(rotationZ_m);
}

void OpalLocalCartesianOffset::update() {
    // getOpalName() comes from AbstractObjects/Object.h
    Offset *offset = dynamic_cast<Offset*>(getElement()->removeWrappers());
    std::string name = getOpalName();

    Vector_t pos;
    if (itsAttr[END_POSITION_Y]) {
        pos[0] = -Attributes::getReal(itsAttr[END_POSITION_Y]);
    }
    if (itsAttr[END_POSITION_Z]) {
        pos[1] = Attributes::getReal(itsAttr[END_POSITION_Z]);
    }
    if (itsAttr[END_POSITION_X]) {
        pos[2] = Attributes::getReal(itsAttr[END_POSITION_X]);
    }

    if (itsAttr[END_ROTATION_X]) {
        rotationZ_m = Attributes::getReal(itsAttr[END_ROTATION_X]);
    }
    if (itsAttr[END_ROTATION_Y]) {
        rotationX_m = Attributes::getReal(itsAttr[END_ROTATION_Y]);
    }
    if (itsAttr[END_ROTATION_Z]) {
        rotationY_m = Attributes::getReal(itsAttr[END_ROTATION_Z]);
    }

    bool hasRotation = itsAttr[END_ROTATION_X] || 
                       itsAttr[END_ROTATION_Y] || 
                       itsAttr[END_ROTATION_Z];
    if (itsAttr[END_NORMAL_X] || itsAttr[END_NORMAL_Y]) {
        if (hasRotation) {
            throw OpalException("OpalLocalCartesianOffset::update",
                "Found END NORMAL and END ROTATION on an offset - either define"
                " one or the other but not both.");

        }
        double endX = Attributes::getReal(itsAttr[END_NORMAL_X]);
        double endY = -Attributes::getReal(itsAttr[END_NORMAL_Y]);
        rotationY_m = atan2(endY, endX);
    }

    Rotation3D rotation = Rotation3D::ZRotation(rotationZ_m)
                         *Rotation3D::YRotation(rotationY_m)
                         *Rotation3D::XRotation(rotationX_m);
    Offset off = Offset(Offset::localCartesianOffset(name, pos, rotation));
    *offset = off;
    setElement(offset->makeAlignWrapper());
}
}
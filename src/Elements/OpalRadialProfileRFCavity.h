/* 
 *  Copyright (c) 2014, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions are met: 
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer. 
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to 
 *     endorse or promote products derived from this software without specific 
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef OPAL_OPALRADIALPROFILERFCAVITY_H
#define OPAL_OPALRADIALPROFILERFCAVITY_H

#include "Elements/OpalElement.h"

class RadialProfileRFCavity;

/** OpalRadialProfileRFCavity
 */
class OpalRadialProfileRFCavity: public OpalElement {
  public:
    /** enum maps string to integer value for UI definitions */
    enum {
        PHASE = COMMON,
        FREQUENCY,
        HEIGHT,
        PROFILE_FILENAME,
        IS_VERTICAL,
        SIZE // size of the enum
    };

    /** Copy constructor **/
    OpalRadialProfileRFCavity(const std::string &name, OpalRadialProfileRFCavity *parent);

    /** Default constructor **/
    OpalRadialProfileRFCavity();

    /** Inherited copy constructor
     *
     *  Call on a base class to instantiate an object of derived class's type
    **/
    OpalRadialProfileRFCavity* clone();

    /** Inherited copy constructor
     *
     *  Call on a base class to instantiate an object of derived class's type
     */
    virtual OpalRadialProfileRFCavity *clone(const std::string &name);

    /** Destructor does nothing */
    virtual ~OpalRadialProfileRFCavity();

    /** Fill in all registered attributes
     *
     *  This updates the registered attributed with values from the ElementBase
     */
    virtual void fillRegisteredAttributes(const ElementBase &, ValueFlag);

    /** Update the OpalRadialProfileRFCavity with new parameters from UI parser */
    virtual void update();

  private:
    // Not implemented.
    OpalRadialProfileRFCavity(const OpalRadialProfileRFCavity &);
    void operator=(const OpalRadialProfileRFCavity &);
    void getProfileFromFile(RadialProfileRFCavity* cav);
    void getProfileFromAttr(RadialProfileRFCavity* cav);
    
    static const std::string doc_string;

    std::string fname_m="";
};

#endif // OPAL_OPALRADIALPROFILERFCAVITY_H

/*
 *  Copyright (c) 2012, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include "Elements/OpalRadialProfileRFCavity.h"

#include <fstream>

#include "Physics/Physics.h"
#include "Utilities/OpalException.h"
#include "Attributes/Attributes.h"
#include "Algorithms/AbstractTimeDependence.h"
#include "AbsBeamline/RadialProfileRFCavity.h"



extern Inform *gmsg;

const std::string OpalRadialProfileRFCavity::doc_string =
      std::string("The \"RADIAL_PROFILE_RF_CAVITY\" element defines an RF cavity ")+
      std::string("with amplitude dependent on radius.");

OpalRadialProfileRFCavity::OpalRadialProfileRFCavity():
    OpalElement(SIZE, "RADIAL_PROFILE_RF_CAVITY", doc_string.c_str()) {
    itsAttr[HEIGHT] = Attributes::makeReal("HEIGHT",
                "Full height of the cavity [m].");
    itsAttr[PHASE] = Attributes::makeReal("PHASE",
                "The cavity phase in [rad].");
    itsAttr[FREQUENCY] = Attributes::makeReal("FREQUENCY",
                "The cavity frequency in [MHz].");
    itsAttr[PROFILE_FILENAME] = Attributes::makeString("PROFILE_FILENAME",
                "The filename of list of points; should be column with RADIUS AMPLITUDE one per row.");
    itsAttr[IS_VERTICAL] = Attributes::makeReal("IS_VERTICAL",
                "Set to non-zero to profile in vertical direction (rather than horizontal)");
    registerRealAttribute("HEIGHT");
    registerRealAttribute("PHASE");
    registerRealAttribute("FREQUENCY");
    registerStringAttribute("PROFILE_FILENAME");
    registerRealAttribute("IS_VERTICAL");

    registerOwnership();

    setElement((new RadialProfileRFCavity("RADIAL_PROFILE_RF_CAVITY"))->makeAlignWrapper());
}

OpalRadialProfileRFCavity::OpalRadialProfileRFCavity(const std::string &name,
                                           OpalRadialProfileRFCavity *parent) :
          OpalElement(name, parent) {
    RadialProfileRFCavity *cavity = dynamic_cast<RadialProfileRFCavity*>(
                                        parent->getElement()->removeWrappers());
    fname_m = parent->fname_m;
    setElement((new RadialProfileRFCavity(*cavity))->makeAlignWrapper());
}

OpalRadialProfileRFCavity::~OpalRadialProfileRFCavity() {
}

OpalRadialProfileRFCavity *OpalRadialProfileRFCavity::clone(const std::string &name) {
    OpalRadialProfileRFCavity* rf = new OpalRadialProfileRFCavity(name, this);
    rf->fname_m = this->fname_m;
    return rf;
  
}

OpalRadialProfileRFCavity *OpalRadialProfileRFCavity::clone() {
    OpalRadialProfileRFCavity* rf = new OpalRadialProfileRFCavity(this->getOpalName(), this);
    rf->fname_m = this->fname_m;
    return rf;
}

void OpalRadialProfileRFCavity::
            fillRegisteredAttributes(const ElementBase &base, ValueFlag flag) {
    OpalElement::fillRegisteredAttributes(base, flag);
    const RadialProfileRFCavity* cavity = dynamic_cast<const RadialProfileRFCavity*>(&base);
    if (cavity == NULL) {
        throw OpalException("OpalRadialProfileRFCavity::fillRegisteredAttributes",
                            "Failed to cast ElementBase to a RadialProfileRFCavity");
    }

    attributeRegistry["L"]->setReal(cavity->getLength());
    attributeRegistry["HEIGHT"]->setReal(cavity->getHeight());
    attributeRegistry["PHASE"]->setReal(cavity->getPhase());
    attributeRegistry["FREQUENCY"]->setReal(cavity->getFrequency());
    attributeRegistry["PROFILE_FILENAME"]->setString(fname_m);
    attributeRegistry["IS_VERTICAL"]->setReal(cavity->getIsVertical());
}

void OpalRadialProfileRFCavity::update() {
    OpalElement::update();
    RadialProfileRFCavity *cavity = dynamic_cast<RadialProfileRFCavity*>(
                                                getElement()->removeWrappers());
    double length = Attributes::getReal(itsAttr[LENGTH]);
    cavity->setLength(length);
    double height = Attributes::getReal(itsAttr[HEIGHT]);
    cavity->setHeight(height);
    double phase = Attributes::getReal(itsAttr[PHASE]);
    cavity->setPhase(phase);
    double frequency = Attributes::getReal(itsAttr[FREQUENCY]);
    cavity->setFrequency(frequency);
    fname_m = Attributes::getString(itsAttr[PROFILE_FILENAME]);
    getProfileFromFile(cavity);
    bool vertical = bool(Attributes::getReal(itsAttr[IS_VERTICAL]));
    cavity->setIsVertical(vertical);

    setElement(cavity->makeAlignWrapper());
}

void OpalRadialProfileRFCavity::getProfileFromAttr(RadialProfileRFCavity* cavity) {
    std::vector<double> radii;// = Attributes::getRealArray(itsAttr[RADIAL_PROFILE]);
    std::vector<double> amps;// = Attributes::getRealArray(itsAttr[AMPLITUDE_PROFILE]);
    RadialProfileRFCavity::profile_vector profile;
    if (radii.size() != amps.size()) {
        throw OpalException("OpalRadialProfileRFCavity::update",
              "Different number of radii points to amplitude points in voltage profile");
    }
    for (size_t i = 0; i < radii.size(); ++i) {
        std::pair<double, double> point(radii[i], amps[i]);
        profile.push_back(point);
    }
    cavity->setAmplitudeProfile(profile);
}

void OpalRadialProfileRFCavity::getProfileFromFile(RadialProfileRFCavity* cavity) {
    if (fname_m == "") {
        // maybe the element is initialising off default data?
        return;
    }
    std::ifstream fin(fname_m.c_str());
    if (!fin) {
        throw OpalException("OpalRadialProfileRFCavity::readProfileFile",
              "Failed to open RadialProfile file '"+fname_m+"'");
    }
    std::string line = "a";
    RadialProfileRFCavity::profile_vector profile;
    while (fin) {
        std::pair<double, double> point;
        fin >> point.first >> point.second;
        if (!fin) {
            break;
        }
        profile.push_back(point);
    }
    cavity->setAmplitudeProfile(profile);
}

/* 
 *  Copyright (c) 2017, Chris Rogers
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions are met: 
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer. 
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 *  3. Neither the name of STFC nor the names of its contributors may be used to 
 *     endorse or promote products derived from this software without specific 
 *     prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include <boost/algorithm/string.hpp>

// Classic includes:
#include "AbsBeamline/EndFieldModel/Tanh.h"
#include "AbsBeamline/EndFieldModel/Enge.h"
#include "AbsBeamline/EndFieldModel/Arctan.h"

#include "AbsBeamline/BoundingBox/Cuboid.h"
#include "AbsBeamline/BoundingBox/CubicHexahedron.h"

#include "AbsBeamline/VerticalFFAMagnet.h"

// OPAL includes:
#include "Attributes/Attributes.h"
#include "Utilities/OpalException.h"  // used?
#include "Elements/OpalVerticalFFAMagnet.h"

extern Inform *gmsg;

std::string OpalVerticalFFAMagnet::docstring_m = 
      std::string("The \"VerticalFFAMagnet\" element defines a vertical FFA ")+
      std::string("magnet, which has a field that increases in the vertical ")+
      std::string("direction while maintaining similar orbits.");
OpalVerticalFFAMagnet::OpalVerticalFFAMagnet() :
    OpalElement(SIZE, "VERTICALFFAMAGNET", docstring_m.c_str()) {
    itsAttr[B0] = Attributes::makeReal
          ("B0", "The nominal dipole field of the magnet at zero height [T].");
    itsAttr[FIELD_INDEX] = Attributes::makeReal("FIELD_INDEX",
      "Exponent term in the field index [m^(-1)].");
    itsAttr[WIDTH] = Attributes::makeReal("WIDTH",
      "The full width of the magnet. Particles moving more than WIDTH/2 horizontally, in either direction, are out of the aperture.");
    itsAttr[MAX_HORIZONTAL_POWER] = Attributes::makeReal("MAX_HORIZONTAL_POWER",
      "The maximum power in horizontal coordinate that will be considered in the field expansion.");
    itsAttr[END_LENGTH] = Attributes::makeReal("END_LENGTH",
      "The end length of the FFA fringe field [m].");
    itsAttr[CENTRE_LENGTH] = Attributes::makeReal("CENTRE_LENGTH",
      "The centre length of the FFA (i.e. length of the flat top) [m].");
    itsAttr[ENGE_PARAMETERS] = Attributes::makeRealArray("ENGE_PARAMETERS",
      "Array of parameters to Enge.");
    itsAttr[BB_LENGTH] = Attributes::makeReal("BB_LENGTH",
      "Determines the length of the bounding box. Magnet is situated symmetrically in the bounding box. [m]");
    itsAttr[HEIGHT_POS_EXTENT] = Attributes::makeReal("HEIGHT_POS_EXTENT",
      "Height of the magnet above z=0. Particles moving upwards more than HEIGHT_POS_EXTENT are out of the aperture [m].");
    itsAttr[HEIGHT_NEG_EXTENT] = Attributes::makeReal("HEIGHT_NEG_EXTENT",
      "Height of the magnet below z=0. Particles moving downwards more than HEIGHT_NEG_EXTENT are out of the aperture [m].");
    itsAttr[END_FIELD_MODEL] = Attributes::makeString("END_FIELD_MODEL",
      "OPAL will use the named end field model, either tanh, arctan, enge (Default: tanh)");
    itsAttr[BB_START_NORMAL] = Attributes::makeRealArray("BB_START_NORMAL",
      "Three-vector containing the normal to the bounding box opening plane.");
    itsAttr[BB_END_NORMAL] = Attributes::makeRealArray("BB_END_NORMAL",
      "Three-vector containing the normal to the bounding box opening plane.");
    itsAttr[BB_START_POSITION] = Attributes::makeRealArray("BB_START_POSITION",
      "Three-vector containing the normal to the bounding box opening plane.");
    itsAttr[BB_END_POSITION] = Attributes::makeRealArray("BB_END_POSITION",
      "Three-vector containing the normal to the bounding box opening plane.");
    registerRealAttribute("B0");
    registerRealAttribute("FIELD_INDEX");
    registerRealAttribute("WIDTH");
    registerRealAttribute("MAX_HORIZONTAL_POWER");
    registerRealAttribute("END_LENGTH");
    registerRealAttribute("CENTRE_LENGTH");
    registerRealAttribute("BB_LENGTH");
    registerRealAttribute("HEIGHT_NEG_EXTENT");
    registerRealAttribute("HEIGHT_POS_EXTENT");
    registerStringAttribute("END_FIELD_MODEL");
    registerOwnership();

    VerticalFFAMagnet* magnet = new VerticalFFAMagnet("VerticalFFAMagnet");
    magnet->setEndField(new endfieldmodel::Tanh(1., 1., 1));
    setElement(magnet->makeAlignWrapper());
}


OpalVerticalFFAMagnet::OpalVerticalFFAMagnet(const std::string &name,
                                             OpalVerticalFFAMagnet *parent) :
    OpalElement(name, parent) {
    VerticalFFAMagnet* magnet = new VerticalFFAMagnet(name);
    magnet->setEndField(new endfieldmodel::Tanh(1., 1., 1));
    setElement(magnet->makeAlignWrapper());
}


OpalVerticalFFAMagnet::~OpalVerticalFFAMagnet() {
}


OpalVerticalFFAMagnet *OpalVerticalFFAMagnet::clone(const std::string &name) {
    return new OpalVerticalFFAMagnet(name, this);
}


void OpalVerticalFFAMagnet::
fillRegisteredAttributes(const ElementBase &base, ValueFlag flag) {
    OpalElement::fillRegisteredAttributes(base, flag);
}


void OpalVerticalFFAMagnet::update() {
    VerticalFFAMagnet *magnet = 
              dynamic_cast<VerticalFFAMagnet*>(getElement()->removeWrappers());
    std::cerr << "OpalVerticalFFAMagnet::update " << magnet << std::endl;
    magnet->setB0(Attributes::getReal(itsAttr[B0]));
    int maxOrder = floor(Attributes::getReal(itsAttr[MAX_HORIZONTAL_POWER]));
    magnet->setMaxOrder(maxOrder);
    magnet->setFieldIndex(Attributes::getReal(itsAttr[FIELD_INDEX]));
    getBB();
    std::string endFieldModel = "tanh";
    if (itsAttr[END_FIELD_MODEL]) {
      endFieldModel = Attributes::getString(itsAttr[END_FIELD_MODEL]);
      boost::to_lower(endFieldModel);
    }
    if (endFieldModel == "tanh") {
        tanhEndField(maxOrder);
    } else if (endFieldModel == "arctan") {
        arctanEndField();
    } else if (endFieldModel == "enge") {
        engeEndField(maxOrder);
    } else {
        throw(OpalException("OpalVerticalFFAMagnet::update()", "Did not recognise vffa end_field_model '"+
              endFieldModel+"'. It should be one of 'tanh', 'arctan', 'enge'"));
    }

    magnet->initialise();
    double mm = 1e3;
    double width = magnet->getWidth();
    double height = 2*std::max(magnet->getNegativeVerticalExtent(),
                               magnet->getPositiveVerticalExtent())*mm;
    std::vector<double> aperture = {height, width};
    magnet->setAperture(ElementBase::RECTANGULAR, aperture);
    OpalElement::update();
    magnet->setAperture(ElementBase::RECTANGULAR, aperture);
    setElement(magnet->makeAlignWrapper());
}

void OpalVerticalFFAMagnet::getBB() {
    VerticalFFAMagnet *magnet = 
              dynamic_cast<VerticalFFAMagnet*>(getElement()->removeWrappers());
    magnet->setNegativeVerticalExtent(Attributes::getReal(itsAttr[HEIGHT_NEG_EXTENT]));
    magnet->setPositiveVerticalExtent(Attributes::getReal(itsAttr[HEIGHT_POS_EXTENT]));
    magnet->setWidth(Attributes::getReal(itsAttr[WIDTH]));
    magnet->setBBLength(Attributes::getReal(itsAttr[BB_LENGTH]));
    boundingbox::Cuboid * cube = new boundingbox::Cuboid(
      -Attributes::getReal(itsAttr[WIDTH])/2, Attributes::getReal(itsAttr[WIDTH])/2,
      -Attributes::getReal(itsAttr[HEIGHT_NEG_EXTENT]), Attributes::getReal(itsAttr[HEIGHT_POS_EXTENT]),
      0.0, Attributes::getReal(itsAttr[BB_LENGTH])
    );

    if (!itsAttr[BB_START_NORMAL]) {
        magnet->setBoundingBox(cube);
        return;
    }
    Vector_t startNormal = getVector(itsAttr[BB_START_NORMAL]);
    Vector_t endNormal = getVector(itsAttr[BB_END_NORMAL]);
    Vector_t startPosition = getVector(itsAttr[BB_START_POSITION]);
    Vector_t endPosition = getVector(itsAttr[BB_END_POSITION]);
    boundingbox::CubicHexahedron* cubehex = new boundingbox::CubicHexahedron(
        cube, startPosition, startNormal, endPosition, endNormal
    );
    std::cerr << "Made CubicHex BB with " << startPosition << " " << startNormal << "\n          "
              << endPosition << " " << endNormal << std::endl;
    magnet->setBoundingBox(cubehex);
    return;

}


Vector_t OpalVerticalFFAMagnet::getVector(Attribute att) {
    std::vector<double> c_vector = Attributes::getRealArray(att);
    if (c_vector.size() != 3) {
        throw OpalException("OpalVerticalFFAMagnet::getVector",
                            "Expected real array of size 3");
    }
    return Vector_t(c_vector[0], c_vector[1], c_vector[2]);
}


void OpalVerticalFFAMagnet::tanhEndField(int maxOrder) {
    // get centre length and end length
    endfieldmodel::Tanh* endField = new endfieldmodel::Tanh();
    double mm = 1e3;
    double end_length = Attributes::getReal(itsAttr[END_LENGTH])*mm;
    double centre_length = Attributes::getReal(itsAttr[CENTRE_LENGTH])*mm/2.;
    endField->setLambda(end_length);
    // x0 is the distance between B=0.5*B0 and B=B0 i.e. half the centre length
    endField->setX0(centre_length/2.);
    endField->setTanhDiffIndices(maxOrder+2);

    VerticalFFAMagnet *magnet = 
              dynamic_cast<VerticalFFAMagnet*>(getElement()->removeWrappers());
    magnet->setEndField(endField);
}


void OpalVerticalFFAMagnet::arctanEndField() {

    // get centre length and end length
    double mm = 1e3;
    double endLength = Attributes::getReal(itsAttr[END_LENGTH])*mm;
    double centreLength = Attributes::getReal(itsAttr[CENTRE_LENGTH])*mm;
    endfieldmodel::Arctan* endField = 
                new endfieldmodel::Arctan(centreLength, endLength);

    VerticalFFAMagnet *magnet = 
              dynamic_cast<VerticalFFAMagnet*>(getElement()->removeWrappers());
    magnet->setEndField(endField);
}

void OpalVerticalFFAMagnet::engeEndField(int maxOrder) {

    // get centre length and end length
    double mm = 1e3;
    double endLength = Attributes::getReal(itsAttr[END_LENGTH])*mm;
    double centreLength = Attributes::getReal(itsAttr[CENTRE_LENGTH])*mm/2.;
    std::vector<double> engeParams = Attributes::getRealArray(itsAttr[ENGE_PARAMETERS]);
    endfieldmodel::Enge* endField =
                          new endfieldmodel::Enge(engeParams, centreLength, endLength, maxOrder);

    VerticalFFAMagnet *magnet = 
              dynamic_cast<VerticalFFAMagnet*>(getElement()->removeWrappers());
    magnet->setEndField(endField);
}

